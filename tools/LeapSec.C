#include <TApplication.h>
#include <Riostream.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TLegend.h>
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TEntryList.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TClass.h>

#include <TClock.h>
#include <TCli.h>
#include <ROOT/IOPlus/Helpers.h>

constexpr Clock TAI = Clock::TAI;
constexpr Clock GPS = Clock::GPS;
constexpr Clock Unix = Clock::Unix;

int main(int argc, char **argv)
{
    int tp;
    TString formatStr;
    bool bGPS;

    std::vector<TString> v;
    TCli& cli = TCli::Instance(&argc, argv);
          cli.SkipHeader();
          cli.AddMessage("This script is used to compute leap seconds from specific GPS time or date..");
          cli.AddOption("--format|-f", "Date format", formatStr);
          cli.AddOption("--time-precision|-tp", "Time precision", tp, 0);
          cli.AddOption("--gps", "Use GPS clock", bGPS);
          cli.AddVariadic(v, "");

    if(!cli.Usage() ) return 1;

    TClockAbstract *clock;
    if(bGPS) clock = gClockGPS;
    else clock = gClock;
    
    bool onlyFloats = true;
    for(int i = 0; i < v.size(); i++) {

        if(v[i].IsFloat()) continue;
        onlyFloats = false;
    }

    if(!onlyFloats) {

        v[0] = ROOT::IOPlus::Helpers::Implode(" ", v);
        v.resize(1);
    }

    if(!v.size()) {
        v.push_back(Form("%f", clock->Now()));
    }

    const char *format = formatStr.EqualTo("") ? nullptr : formatStr.Data();
    for(int i = 0; i < v.size(); i++) {
        if(!v[i].IsFloat()) v[i] = Form("%f", clock->Parse(v[i], format));
    }

    for(int i = 0; i < v.size(); i++) {

        double timestamp = v[i].Atof();
        if(ROOT::IOPlus::Helpers::IsNaN(timestamp)) {
            
            std::cerr << "Failed to compute timestamp `" << timestamp << "`";
            if(!formatStr.EqualTo("")) std::cerr << " (with format: `" << formatStr << "`)";
            continue;
        }

        int leapSeconds = clock->GetLeapSeconds(timestamp);
        std::cout << TPrint::kGreen << clock->GetName() << " " << std::fixed;
        std::cout << Form("%."+TString::Itoa(tp, 10)+"f", timestamp);
        std::cout << std::dec << TPrint::kNoColor;

        std::cout << " = "<< Form("%."+TString::Itoa(tp, 10)+"f", timestamp - leapSeconds);
        std::cout << " + "<< Form("%."+TString::Itoa(tp, 10)+"f", leapSeconds) << " leap second(s)" << std::endl;
    }

    return 0;
}
