#include <iostream>

class MyClass {
public:
    MyClass() { std::cout << "Default constructor\n"; }
    MyClass(const MyClass&) { std::cout << "Copy constructor\n"; }
    MyClass(MyClass&&) noexcept { std::cout << "Move constructor\n"; }
    MyClass& operator=(const MyClass&) { std::cout << "Copy assignment\n"; return *this; }
    MyClass& operator=(MyClass&&) noexcept { std::cout << "Move assignment\n"; return *this; }
    ~MyClass() { std::cout << "Destructor\n"; }

    static MyClass F(MyClass& cl) {
        return cl; // cl might be copied or moved here
    }
};

int main() {
    MyClass obj;
    MyClass newObj = MyClass::F(obj);
    return 0;
}
