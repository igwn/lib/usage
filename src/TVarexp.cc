/**
 * @file TVarexp.cc
 * @brief Source code of the TVarexp class
 *
 * @details
 * This file contains the implementation of the TVarexp class.
 *
 * @date 2019-03-27
 * @author Marco Meyer <marco.meyer@cern.ch>
 */

#include <time.h>
#include "TVarexp.h"

ClassImp(TVarexp)
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

/** @cond */
const bool TVarexp::kWithBinning = true;
const bool TVarexp::kWithoutBinning = !kWithBinning;
const bool TVarexp::kSwap = true;
/** @endcond */

TString TVarexp::StripBranchName(TString str) {

        TPRegexp r1("([a-zA-Z0-9_]*\\.)");
        while(r1.Substitute(str, "")) {};

        str.ReplaceAll("()", "");
        return str;
}

TString TVarexp::GetTFormulaHistogramName(TH1 *h0, TString suffix, bool bBinning)
{
        if(h0 == NULL) return "";
        if(bBinning != TVarexp::kWithBinning) h0->GetName();

        TString hpar = "";
        TString hname = h0->GetName();

        if(h0->InheritsFrom("TH3")) {

                hpar = Form("(%d,%f,%f, %d,%f,%f, %d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                            h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                            h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );

        } else if(h0->InheritsFrom("TH2")) {

                hpar = Form("(%d,%f,%f, %d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                            h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );

        } else if(h0->InheritsFrom("TH1")) {

                hpar = Form("(%d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax() );
        }

        return TString(hname + suffix + hpar);
}

TString TVarexp::RemoveBinning()
{
        TString hpar = "";

        std::vector<TString> vArray = this->Tokenize(':', kWithoutBinning);
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(i != 0) hpar += ":";
                hpar += vArray[i];
        }

        return hpar;
}

int TVarexp::GetN()
{
        int N = 0;
        TPRegexp regex("\\[[0-9]*\\]\\.");
        Ssiz_t length;

        int index = 0;
        while((index = this->varexp.Index(regex, &length, index)) != -1) {

                index += length;
                N++;
        }

        return N;
}

int TVarexp::GetNdimensions()
{
        if(this->Get().EqualTo("")) return 0;

        std::vector<TString> vArray = this->Tokenize();
        return vArray.size();
}

bool TVarexp::HasParameters(TString varexp0, TString pattern)
{
        TPRegexp regex(pattern);
        return varexp0.Contains(regex);
}

int TVarexp::GetNparameters(TString varexp0, TString pattern)
{
        varexp0 = TVarexp(varexp0).Get(); // TVarexp::Get() removes space..
        if(varexp0.EqualTo("")) return 0;

        int N = 0;
        TPRegexp regex(pattern);
        Ssiz_t length;

        int index = 0;
        while((index = varexp0.Index(regex, &length, index)) != -1) {

                int i = 4;
                if(N > 10) i++;
                TString catch0 = varexp0(index, i);
                varexp0.ReplaceAll(catch0, "");

                N++;
        }

        return N;
}

bool TVarexp::HasBinningInformation(TString varexp0)
{
        if(TVarexp::HasConstantBinningInformation(varexp0)) return 1;
        if(TVarexp::HasVariableBinningInformation(varexp0)) return 1;

        return 0;
}

bool TVarexp::HasConstantBinningInformation(TString varexp0)
{
        TPRegexp regex("\\[[^,]*,[^,]*,[^,]*\\]");
        return varexp0.Contains(regex);
}

bool TVarexp::HasVariableBinningInformation(TString varexp0)
{
        TPRegexp regex("\\{.*\\}");
        return varexp0.Contains(regex);
}

std::vector<std::vector<double> > TVarexp::GetConstantBinning(TString varexp0)
{
        std::vector<std::vector<double> > vv;

        std::vector<TString> vArray = TVarexp(varexp0).Tokenize();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                std::vector<TString> v = {
                        vArray[i],
                        vArray[i],
                        vArray[i],
                        vArray[i]
                };

                TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                r.Substitute(v[0], "$1");
                r.Substitute(v[1], "$2");
                r.Substitute(v[2], "$3");
                r.Substitute(v[3], "$4");

                if(v[0] != v[1] && v[0] != v[2] && v[0] != v[3]) {

                        int nbins = ROOT::IOPlus::Helpers::InterpretFormula(v[1]);
                        double xmin = ROOT::IOPlus::Helpers::InterpretFormula(v[2]);
                        double xmax = ROOT::IOPlus::Helpers::InterpretFormula(v[3]);
                        double delta = (xmax-xmin)/nbins;

                        std::vector<double> v0;
                        for(int i = 0; i < nbins+1; i++)
                                v0.push_back(xmin+i*delta);

                        vv.push_back(v0);
                }

                else vv.push_back({});
        }

        return vv;
}

std::vector<std::vector<double> > TVarexp::GetVariableBinning(TString varexp0)
{
        std::vector<std::vector<double> > vv;
        std::vector<TString> vArray = TVarexp(varexp0).Tokenize(':', TVarexp::kWithBinning);

        for(int i = 0, N = vArray.size(); i < N; i++) {

                TString varexp0;

                TString varname_str = vArray[i];
                int brackleft = varname_str.First('{');
                if(brackleft != -1) {

                        int brackright = varname_str.Last('}');
                        if(brackright != -1) {

                                int length = brackright-brackleft-1;
                                if(length >= 0) varexp0 = varname_str(brackleft+1,length);
                        }
                }

                std::vector<double> v0;
                std::vector<TString> v = TVarexp(varexp0).Tokenize(',');
                for(int i = 0, N = v.size(); i < N; i++)
                        v0.push_back(ROOT::IOPlus::Helpers::InterpretFormula(v[i]));

                vv.push_back(v0);
        }

        return vv;
}

TString TVarexp::GetVariableBinningStr(bool bBrackets)
{
        unsigned int count = 0;
        TString hpar = "";

        std::vector<std::vector<double> > vArray = this->GetVariableBinning();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(i > 0) hpar += ":";

                if(bBrackets) hpar += "{";

                for(int j = 0, J = vArray[i].size(); j < J; j++) {

                        if(j > 0) hpar += ",";
                        hpar += Form("%f", vArray[i][j]);
                }

                if(bBrackets) hpar += "}";
        }

        TPrint::WarningIf(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "You have to properly specify all bin ranges.."
                );

        TPrint::WarningIf(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "If you want your histogram within your expected range"
                );

        return hpar;
}

TString TVarexp::GetConstantBinningStr(bool bBrackets)
{
        unsigned int count = 0;
        TString hpar = "";

        std::vector<TString> vArray = this->Tokenize();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(HasVariableBinningInformation(vArray[i]))

                        TPrint::Error(__METHOD_NAME__, "This function handles only constant bins..");
                else {

                        //
                        // Constant binning case
                        //
                        TString varname_str = vArray[i];
                        TString nbins_str   = vArray[i];
                        TString binmin_str  = vArray[i];
                        TString binmax_str  = vArray[i];

                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$1");
                        r.Substitute(nbins_str, "$2");
                        r.Substitute(binmin_str, "$3");
                        r.Substitute(binmax_str, "$4");

                        if(varname_str != nbins_str && varname_str != binmin_str && varname_str != binmax_str) {

                                if(!hpar.EqualTo("")) hpar = "," + hpar;
                                hpar = Form("%d,%f,%f", (int) ROOT::IOPlus::Helpers::InterpretFormula(nbins_str.Data()), ROOT::IOPlus::Helpers::InterpretFormula(binmin_str.Data()), ROOT::IOPlus::Helpers::InterpretFormula(binmax_str.Data())) + hpar;
                                count++;
                        }
                }
        }

        TPrint::WarningIf(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "You have to properly specify all bin ranges.."
                );

        TPrint::WarningIf(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "If you want your histogram within your expected range"
                );

        if(bBrackets) return "[" + hpar + "]";
        return hpar;
}

TString TVarexp::Expand(std::vector<TTree*> vTree, bool bBinning)
{
        // Replace encoded parameters..
        TString varexp_expanded = "";
        //if(TPrint::Error(!vTree.size() || vTree[0] == NULL, __METHOD_NAME__, "First tree of the list cannot be NULL")) gSystem->Abort();

        if(!vTree.size()) return varexp;
        if(!vTree[0]) return varexp;

        if(!vTree[0]->InheritsFrom("TTree")) {
                TPrint::Error(__METHOD_NAME__, "Can only expand tree objects.. (index #0 is not..)");
                return varexp;
        }

        std::vector<TString> vArray = this->Tokenize(':', bBinning);
        for(int i = 0, N = vArray.size(); i < N; i++) {

                TString varexp0 = vArray[i];

                // Replace default branchname from tree[0] if not bracket found
                if(vTree.size() && vTree[0] != NULL) {

                        if(!vTree[0]->InheritsFrom("TTree")) {
                                TPrint::Error(__METHOD_NAME__, "Can only expand tree objects.. (index #%d is not..)", i);
                                return "";
                        }

                        TList *l = (TList*) vTree[0]->GetListOfBranches();
                        for(int j = 0, N = l->GetSize(); j < N; j++) {

                                if(l->At(j) == NULL) break;
                                TString branchname = l->At(j)->GetName();
                                if(branchname.EndsWith(".") && varexp0.BeginsWith(branchname)) varexp0 = "[0]." + varexp0;
                                else if(varexp0.EqualTo(branchname)) varexp0 = "[0]." + varexp0;
                        }
                }

                // Replace bracket by treename..
                for(int j = 0, J = vTree.size(); j < J; j++) {

                        if(vTree[j] == NULL) continue;
                        varexp0.ReplaceAll(Form("[%d].",j), (TString) vTree[j]->GetName() + ".");
                }

                if(i==0) varexp_expanded = varexp0;
                else varexp_expanded += ":" + varexp0;
        }

        // Check if there is still an encoded parameter
        if(HasParameters((varexp_expanded))) {

                TPrint::Warning(__METHOD_NAME__, Form("The expression \"%s\" still have encoded parameters..", varexp_expanded.Data()));
                TPrint::Warning(__METHOD_NAME__, Form("Only %d were taken into account.. Something might be wrong there..", (int) vTree.size()));
                gSystem->Abort();
        }

        //Look for lonely branches and put the first treename in front of it.
        if(vTree.size() && vTree[0]) {

                TList *l = (TList*) vTree[0]->GetListOfBranches();
                for(int i = 0, N = l->GetSize(); i < N; i++) {

                        if(l->At(i) == NULL) break;
                        TString branchname = l->At(i)->GetName();
                        Ssiz_t length;

                        int index = 0;
                        while((index = varexp_expanded.Index(branchname, &length, (Ssiz_t) index)) != -1) {

                                int index0 = index;
                                TString before = (index > 0) ? (TString) varexp_expanded(index-1,1) : "";
                                TString after = (TString) varexp_expanded(index+length,1);

                                TString word = (TString) varexp_expanded(index,varexp_expanded.Length());

                                index += branchname.Length();

                                //cout << branchname << " " << word << std::endl;
                                //cout << "before : \""<<before << "\"" << std::endl;
                                //cout << "after : \""<<after << "\"" << std::endl;
                                if(before.EqualTo(".")) continue;

                                TPRegexp r1("[a-zA-Z0-9]");
                                if(before.Contains(r1)) continue;
                                if(!branchname.EndsWith(".") && after.Contains(r1)) continue;

                                TString replacement = (TString) vTree[0]->GetName() + ".";
                                varexp_expanded.Replace(index0, 0, replacement);
                                index += replacement.Length();

                                //cout << varexp_expanded << std::endl << std::endl;
                        }

                        // In case we are looking at the regular variable without operations..
                        //        if(varexp_expanded.BeginsWith("^" + branchname + "[^a-zA-Z0-9]")) varexp_expanded = (TString) vTree[0]->GetName() + "." + varexp_expanded;
                }
        }

        TPrint::Debug(30, __METHOD_NAME__, "[Before] : \"" + this->Get() + "\"");
        TPrint::Debug(30, __METHOD_NAME__, "[After]  : \"" + varexp_expanded + "\"");
        return varexp_expanded;
}

TString TVarexp::GetTFormulaHistogramBinning(TH1 *h0, int axis)
{
        if(h0 == NULL) return "";

        TString hname = h0->GetName();
        TString hpar = "";

        if(h0->InheritsFrom("TH3")) {

                if (axis == 1) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax());
                else if (axis == 2) hpar = Form("%d,%f,%f", h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax());
                else if (axis == 3) hpar = Form("%d,%f,%f", h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax());
                else {

                        hpar = Form("%d,%f,%f, %d,%f,%f, %d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );
                }

        } else if(h0->InheritsFrom("TH2")) {

                if (axis == 1) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax());
                else if (axis == 2) hpar = Form("%d,%f,%f", h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax());
                else {

                        hpar = Form("%d,%f,%f, %d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );
                }

        } else if(h0->InheritsFrom("TH1")) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax() );

        return hpar;
}


TString TVarexp::GetTFormulaHistogramBinning(TH1 *h0, TString axis) {

        if(axis.EqualTo("x")) return GetTFormulaHistogramBinning(h0, 1);
        else if(axis.EqualTo("y")) return GetTFormulaHistogramBinning(h0, 2);
        else if(axis.EqualTo("z")) return GetTFormulaHistogramBinning(h0, 3);
        else return GetTFormulaHistogramBinning(h0);
}

// DO NOT CONFUSE WITH BINNING INFORMATION FROM TVAREXP.. TVAREXP IS [x,y,z] while Histogram are (x,y,z)
TString TVarexp::RemoveTFormulaHistogramBinning(TString hname)
{
        TPRegexp r3("(.*)\\(([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)\\)");
        if(r3.Substitute(hname, "$1")) return hname;

        TPRegexp r2("(.*)\\(([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)\\)");
        if(r2.Substitute(hname, "$1")) return hname;

        TPRegexp r1("(.*)\\(([^,]*),([^,]*),([^,]*)\\)");
        if(r1.Substitute(hname, "$1")) return hname;

        return hname;
}

bool TVarexp::HasTFormulaBinningInformation(TString hname)
{
        bool b = false;

        //1D histogram
        TPRegexp regex1("\\([^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex1);
        if(b) return 1;

        //2D histogram
        TPRegexp regex2("\\([^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex2);
        if(b) return 1;

        //3D histogram;
        TPRegexp regex3("\\([^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex3);
        if(b) return 1;

        return 0;
}

TFormula* TVarexp::GetFormula(TString name)
{
        if(name.EqualTo("")) name = ROOT::IOPlus::Helpers::GetRandomName("formula");
        name.ReplaceAll(":", "_");

        return new TFormula(name, this->varexp);
}

std::vector<double> TVarexp::Eval(TFormula *formula, std::vector<double> params, std::vector<double> sigmas, double x, double y, double z, double t)
{
        int nParams = TMath::Max(params.size(), sigmas.size());
        params.resize(nParams, NAN);
        sigmas.resize(nParams, 0);
       
        if(formula == NULL) {

                params.push_back(NAN);
                return params;
        }
 
        for(int i = 0, N = params.size(); i < N; i++) {
        
                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(sigmas[i], 0.0))
                        params[i] = gRandom->Gaus(params[i], sigmas[i]);
        }

        formula->SetParameters(&params[0]);

        double value = 0;
        if(!ROOT::IOPlus::Helpers::IsNaN(t)) value = formula->Eval(x,y,z,t);
        if(!ROOT::IOPlus::Helpers::IsNaN(z)) value = formula->Eval(x,y,z);
        if(!ROOT::IOPlus::Helpers::IsNaN(y)) value = formula->Eval(x,y);
        if(!ROOT::IOPlus::Helpers::IsNaN(x)) value = formula->Eval(x);

        params.push_back(value);
        return params;
}

TString TVarexp::FlipExpression(TString varexp0, char delimiter) {

        std::vector<TString> v0 = TVarexp(varexp0).Tokenize(delimiter);
        std::vector<TString> v = TVarexp::FlipExpression(v0);

        TString varexp;
        for(int i = 0, N = v.size(); i < N; i++)
                varexp += (i == 0) ? v[0] : delimiter + v[i];

        return varexp;
}

std::vector<TString> TVarexp::FlipExpression(std::vector<TString> v)
{
        std::reverse(v.begin(),v.end());
        return v;
}

TString TVarexp::Detokenize(std::vector<double> vStr, char separator) {

        TString str;
        for(int i = 0, N = vStr.size(); i < N; i++) {

                if(i) str += separator;
                str += Form("%f", vStr[i]);
        }

        return str;
}

TString TVarexp::Detokenize(std::vector<int> vStr, char separator) {

        TString str;
        for(int i = 0, N = vStr.size(); i < N; i++) {

                if(i) str += separator;
                str += Form("%d", vStr[i]);
        }

        return str;
}

std::vector<TString> TVarexp::Tokenize(char separator, bool bBinning) {

        std::vector<TString> vArray;

        if(this->varexp.EqualTo("")) return {};

        std::vector<int> vIndex = {0};
        for(int i = 0, N = this->varexp.Length(); i < N; i++) {

                if(TString(this->varexp[i]).EqualTo(separator))
                        vIndex.push_back(i+1);
        }

        vIndex.push_back(this->varexp.Length()+1);
        for(int i = 0, N = vIndex.size()-1; i < N; i++) {

                TString varexp0 = this->varexp(vIndex[i], vIndex[i+1]-vIndex[i]-1);
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(bBinning != TVarexp::kWithBinning) {

                        TString varname_str;

                        //
                        // Remove constant binning
                        //
                        varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$1");
                        varexp0 = varname_str;

                        //
                        // Remove variable binning
                        //
                        varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) varexp0 = varname_str(0,brackleft);
                                }
                        }
                }

                vArray.push_back(varexp0);
        }

        if(!vArray.size()) vArray.push_back("");
        return vArray;
}


std::vector<TString> TVarexp::TokenizeLabel(char separator) {

        return Tokenize(separator, kWithoutBinning);
}

std::vector<int> TVarexp::TokenizeNbins(char separator) {

        std::vector<int> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                std::vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = TString::Itoa(v.size()-1,10);
                                        }
                                }
                        }

                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back((int) ROOT::IOPlus::Helpers::InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(0);
        return vArray;
}

std::vector<double> TVarexp::TokenizeXmin(char separator) {

        std::vector<double> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                std::vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = v[0];
                                        }
                                }
                        }
                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back(ROOT::IOPlus::Helpers::InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(NAN);
        return vArray;
}

std::vector<double> TVarexp::TokenizeXmax(char separator) {

        std::vector<double> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                std::vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = v[v.size()-1];
                                        }
                                }
                        }

                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back(ROOT::IOPlus::Helpers::InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(NAN);
        return vArray;
}

std::vector< std::vector<double> > TVarexp::GetSelection(TTree* tree, TString selection_str, Long64_t nEntries, int firstEntry, int downsample)
{
        std::vector< std::vector<double> > selection;
        std::vector<TString> branches = this->Tokenize();

        int size = TMath::Max((int) branches.size(), 1);
        selection.resize(size);

        if(tree == NULL) return selection;

        // Use RDataFrame..
        // Fixme: handle dynamic branches
        // if(selection_str.EqualTo("")) selection_str = "true";
        // if(tree->GetCurrentFile() != NULL) {

        //         ROOT::RDataFrame df(*tree);                         
        //         for(int i = 0, N = branches.size(); i < N; i++) 
        //         {
        //                 auto v = df.Filter(selection_str.Data()).Take<double>(branches[i]).GetValue();
        //                 if(downsample == 1) selection.push_back(v);
        //                 else {

        //                         for(int j = 0, N = v.size(); j < N/downsample; j++)
        //                                 selection[i].push_back(v[j*downsample]);
        //                 }
        //         }

        //         return selection;
        // }

        int makeClass = tree->GetMakeClass();
        tree->SetMakeClass(0);
        tree->SetEstimate(_selectionBuffer);

        Long64_t nTotalEntries = nEntries < 0 ? tree->GetEntries() : nEntries;

        Long64_t iEntry = nEntries < 0 ? _selectionBuffer : TMath::Min(_selectionBuffer, nEntries);
        while(firstEntry < nTotalEntries) {

                TPrint::RedirectTTY(__METHOD_NAME__);
                tree->Draw(this->varexp, selection_str, "goff", iEntry, firstEntry);
                TString out, err;
                TPrint::GetTTY(__METHOD_NAME__, &out, &err);

                if(err.Contains("The selected TTree subset is empty")) err = "";
                if(!out.EqualTo("")) TPrint::Message(__METHOD_NAME__, out);
                if(!err.EqualTo("")) {

                        TPrint::Error(__METHOD_NAME__, err);
                        return selection;
                }

                for(int i = 0, N = branches.size(); i < N; i++) {

                        double *array = tree->GetVal(i);
                        if(array) {

                                for(int j = 0, N = tree->GetSelectedRows(); j < N/downsample; j++) {
                                        selection[i].push_back(array[j*downsample]);
                                }
                        }
                }

                firstEntry += iEntry;
        }

        tree->SetMakeClass(makeClass);

        return selection;
}
