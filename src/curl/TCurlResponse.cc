#include "ROOT/IOPlus/TCurlResponse.h"

TCurlResponse::TCurlResponse(const char *content, int statusCode, TCurlDict headers)
    : content(content), statusCode(statusCode), headers(headers) {}

TCurlResponse::~TCurlResponse() {}

const char *TCurlResponse::GetContent() const
{
    return content;
}

int TCurlResponse::GetStatusCode() const
{
    return statusCode;
}

TCurlDict TCurlResponse::GetHeaders() const
{
    return headers;
}

TString TCurlResponse::GetHeader(TString name) const
{
    auto it = headers.find(name);
    return it != headers.end() ? it->second : "";
}

bool TCurlResponse::Empty() const
{
    return strcmp(content, "\0") == 0;
}
