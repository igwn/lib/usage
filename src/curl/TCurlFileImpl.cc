#include "ROOT/IOPlus/Impl/TCurlFileImpl.h"

char         TCurlFile::Impl::_curl_download_chr = '=';
int          TCurlFile::Impl::_curl_download_width = 20;
const char * TCurlFile::Impl::_curl_download_prefix = "Download..";
const char * TCurlFile::Impl::_curl_download_suffix = "Please wait.";

TCurlFile::Impl::Impl() {
    
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    progressChar   = TCurlFile::Impl::_curl_download_chr;
    progressWidth  = TCurlFile::Impl::_curl_download_width;
    progressPrefix = TCurlFile::Impl::_curl_download_prefix;
    progressSuffix = TCurlFile::Impl::_curl_download_suffix;
}

TCurlFile::Impl::~Impl() {

    if(this->error) {
        delete[] this->error;
        this->error = NULL;
    }

    if (curl && this->GetErrorCode() != CURLE_FAILED_INIT) curl_easy_cleanup(curl);
    curl_global_cleanup();
}

TString TCurlFile::Impl::GetHeader(TString name) const
{
    auto it = headers.find(name);
    return it != headers.end() ? it->second : "";
}


void TCurlFile::Impl::SetProgressPrefix(const char *prefix)
{
    this->progressPrefix = prefix;
}

void TCurlFile::Impl::SetProgressSuffix(const char *suffix)
{
    this->progressSuffix = suffix;
}

void TCurlFile::Impl::SetProgressWidth(int width)
{
    this->progressWidth = width;
}

void TCurlFile::Impl::SetProgressChar(char c)
{
    this->progressChar = c;
}

CURL* TCurlFile::Impl::GetHandler() 
{ 
    return this->curl;
}

CURLcode TCurlFile::Impl::GetErrorCode() const
{
    return this->errorCode;
}

const char *TCurlFile::Impl::GetError() const 
{
    if(this->error) {
        delete[] this->error;
    }

    TString error = curl_easy_strerror(this->errorCode);
    this->error = new char[error.Length() + 1];
    std::strcpy(this->error, error);

    return this->error;
}


curl_slist *TCurlFile::Impl::_curl_serialize_headers(const TCurlDict &headers, curl_slist *list)
{
    for (auto it = headers.begin(); it != headers.end(); ++it)
        list = curl_slist_append(list, it->first+":"+it->second);

    return list;
}

TCurlDict TCurlFile::Impl::GetHeaders() const 

{
    return this->headers;
}

void TCurlFile::Impl::ClearHeaders() 
{
    this->headers.clear();
}

void TCurlFile::Impl::AddHeader(TString key, TString value) 
{
    this->headers[key] = value;
}

void TCurlFile::Impl::RemoveHeader(TString key) 
{
    auto it = this->headers.find(key);
    if(it != this->headers.end()) this->headers.erase(it);
}

std::shared_ptr<TCurlResponse> TCurlFile::Impl::RequestHeader(std::shared_ptr<TCurlRequest> request)
{
    bool noBody = this->request->noBody;
    this->request->NoBody(true);
    bool hideProgress = this->request->noProgress;
    this->request->HideProgress(true);

    auto response = this->Request(request);
    this->request->NoBody(noBody);
    this->request->HideProgress(hideProgress);

    return response;
}

std::shared_ptr<TCurlResponse> TCurlFile::Impl::Request(std::shared_ptr<TCurlRequest> request, FILE *buffer)
{
    curl_slist *list = nullptr;
    curl_data_t data;
                data.uri = request->uri;
                data.resume_byte_pos = request->resumeBytePos;
                data.prefix = this->progressPrefix;
                data.suffix = this->progressSuffix;
                data.width  = this->progressWidth;
                data.chr    = this->progressChar;

    auto response = std::make_shared<TCurlResponse>();
    if(curl) {

        curl_easy_reset(curl);

        curl_easy_setopt(curl, CURLOPT_URL, request->uri);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, request->verbose ? 1L : 0L);

        if(buffer != nullptr) curl_easy_setopt(curl, CURLOPT_WRITEDATA, buffer);

        if(request->method == TCurlMethod::GET) // CURLOPT_RESUME_FROM_LARGE is only applied for default requests
            curl_easy_setopt(curl, CURLOPT_RESUME_FROM_LARGE, request->resumeBytePos);

        // Retrieve header response
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, TCurlFile::Impl::_curl_parse_headers);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, &response->headers);

        // Progress bar
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, TCurlFile::Impl::_curl_xferinfo_function);
        curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &data);

        // Global options
        curl_easy_setopt(curl, CURLOPT_NOBODY, request->noBody ? 1L : 0L);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, request->noProgress ? 1L : 0L);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, request->followLocation ? 1L : 0L);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, request->userAgent);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, request->ignorePeerValidation ? 0L : 1L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, request->ignorePeerValidation ? 0L : 2L);

        // Change request method
        switch(request->method) {

            default: [[fallthrough]];
            case TCurlMethod::GET:
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
                if(request->parameters != nullptr && strcmp(request->parameters, "\0"))
                    gPrint->Warning(__METHOD_NAME__, "Post field parameters \"%s\" ignored for GET requests.", request->parameters);
                break;

            case TCurlMethod::POST:
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request->parameters);
                break;

            case TCurlMethod::PUT:
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request->parameters);
                break;

            case TCurlMethod::DELETE:
                curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request->parameters);
                break;
        }

        // Send header request
        list = _curl_serialize_headers(this->headers);
        list = _curl_serialize_headers(request->headers, list);
        if(list != nullptr) curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    }

    this->request = request;
    this->errorCode = curl ? curl_easy_perform(curl) : CURLE_FAILED_INIT;
    if(!request->noProgress) gPrint->ClearLine();

    if(curl) {
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response->statusCode);
        if(list != nullptr) curl_slist_free_all(list);
    }

    // Return response depending on error code (not to be confused with HTTP return code)
    this->response = response;
    if (this->errorCode == CURLE_WEIRD_SERVER_REPLY && request->noBody) {
        this->errorCode = CURLE_OK;
        return response;
    }

    if (this->errorCode == CURLE_OK)
        return response;
        
    return NULL;
}

int TCurlFile::Impl::_curl_xferinfo_function(void* clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow)
{
    bool isDownload = dltotal > 0;
    bool isUpload   = ultotal > 0;
    if (!isDownload && !isUpload) return 0;

    curl_data_t* data = (curl_data_t*) clientp;
    if(data) return 0;
    
    dlnow   += data->resume_byte_pos;
    dltotal += data->resume_byte_pos;
    ulnow   += data->resume_byte_pos;
    ultotal += data->resume_byte_pos;
    
    char chr           = data ? data->chr    : TCurlFile::Impl::_curl_download_chr;
    int width          = data ? data->width  : TCurlFile::Impl::_curl_download_width;
    const char* prefix = data ? data->prefix : TCurlFile::Impl::_curl_download_prefix;
    const char* suffix = data ? data->suffix : TCurlFile::Impl::_curl_download_suffix;

    double fraction = 0;
    if (isDownload) fraction = (double) dlnow / dltotal;
    if (isUpload  ) fraction = (double) ulnow / ultotal;

    double mb_total = 0;
    if (isDownload) mb_total = 1e-6*dltotal;
    if (isUpload  ) mb_total = 1e-6*ultotal;

    double mb_now = 0;
    if (isDownload) mb_now = 1e-6*dlnow;
    if (isUpload  ) mb_now = 1e-6*ulnow;

    if(mb_now == mb_total)
        return 0;
    
    int i0 = 0;
    int i  = (int) round(fraction * width);
    int iN = width;

    TString uri = gSystem->BaseName(data->uri);
    if(uri.EqualTo("")) uri = data->uri;
    
    Ssiz_t pos = uri.Index("?");
    if (pos != kNPOS) uri = uri(0, pos);

    std::cout << "\r" << prefix << " `" << uri << "` [";
    while(i0 < iN) {

        if(isDownload) {

            if(i0 < i || i == iN-1) std::cout << chr;
            else if(i0 == i) std::cout << ">";
            else std::cout << " ";

        } else {

            if(i0 < i || i == iN-1) std::cout << " ";
            else if(i0 == i) std::cout << "<";
            else std::cout << chr;
        }

        i0++;
    }

    std::cout << "] ";
    printf("(%3.0f%%, %.2fMB/%.2fMB) ", 100*fraction, mb_now, mb_total);
    std::cout << suffix << "\r";

    fflush(stdout);
    return 0;
}

size_t TCurlFile::Impl::_curl_header_filesize(char *buffer, size_t size, size_t nitems, void *userdata) {

    if (strncasecmp(buffer, "Content-Length:", 15) == 0) {
        long content_length = atol(buffer + 15);
        *(long*)userdata = content_length;
    }
    return nitems * size;
}

size_t TCurlFile::Impl::_curl_parse_headers(char* buffer, size_t size, size_t nitems, TCurlDict* headers) {
    
    size_t totalSize = size * nitems;
    std::string header(buffer, totalSize);
    
    size_t colonPos = header.find(':');
    if (colonPos != std::string::npos) {
    
        TString key = header.substr(0, colonPos).c_str();
        TString value = header.substr(colonPos + 1).c_str();
        value = value.ReplaceAll("\r", "").ReplaceAll("\n", "").Strip(TString::kBoth);
        (*headers)[key] = value;
    }
    
    return totalSize;
}

time_t TCurlFile::Impl::_curl_parse_time(const TString& strtime) {
        
    return static_cast<time_t>(gClock->Timestamp(strtime));
}

