#include "ROOT/IOPlus/std/iomanip.h"

namespace std {


    // Define global variables
    int _limit = 10;
    int limit() { return _limit; }

    char _delimiter = ',';
    char delimiter() { return _delimiter; }

    int _offset = 0;
    int offset() { return _offset; }

    int _line_size = 256;
}