#include "ROOT/IOPlus/TTensorT.h"
#include "ROOT/IOPlus/Impl/TTensorTImpl.h"

ClassImp(ROOT::IOPlus::TTensorT<int>)
ClassImp(ROOT::IOPlus::TTensorT<short>)
ClassImp(ROOT::IOPlus::TTensorT<float>)
ClassImp(ROOT::IOPlus::TTensorT<std::complex<float>>)
ClassImp(ROOT::IOPlus::TTensorT<double>)
ClassImp(ROOT::IOPlus::TTensorT<std::complex<double>>)

namespace ROOT {
namespace IOPlus {

template<typename Element>
TTensorT<Element>::TTensorT() { pImpl = new TTensorT<Element>::Impl(); };

template<typename Element>
TTensorT<Element>::TTensorT(const std::initializer_list<Element>& list) : TTensorT(GetShape(list), FlattenData(list)) {};
template<typename Element>
TTensorT<Element>::TTensorT(const std::initializer_list<std::initializer_list<Element>>& list) : TTensorT(GetShape(list), FlattenData(list)) {};
template<typename Element>
TTensorT<Element>::TTensorT(const std::initializer_list<std::initializer_list<std::initializer_list<Element>>>& list) : TTensorT(GetShape(list), FlattenData(list)) {};
template<typename Element>
TTensorT<Element>::TTensorT(const TTensorT& tensor): TTensorT(tensor.GetImpl()) { }
template<typename Element>
TTensorT<Element>::TTensorT(const TTensorT::Impl& copy) : TTensorT() { *pImpl = copy.alias(); }
template<typename Element>
TTensorT<Element>::TTensorT(const std::vector<int64_t> &shape, const std::vector<Element> &data, const Element& c) 
                 : TTensorT(shape, (void *) &data[0], data.size(), c) { }
template<typename Element>
TTensorT<Element>::TTensorT(const std::vector<Element> &data, const std::vector<int64_t>& shape, const Element& c)
                 : TTensorT(shape, (void *) &data[0], data.size(), c) { }
template<typename Element>
TTensorT<Element>::TTensorT(const std::vector<int64_t> &shape, void *data, const int64_t &length, const Element& c) : TTensorT()
{
    // Create proper size tensor
    this->pImpl->resize_(shape);

    // Copy existing elements into it
    Element* dataPtr = this->GetDataPtr(0);
    std::memcpy(dataPtr, data, length * sizeof(Element));

    // Fill remaining elements with a specific value
    for(int64_t i = length, N = this->GetEntries(); i < N; i++) {

        if constexpr (!std::is_complex_v<Element>) dataPtr[i] = c;
        else {

            dataPtr[2*i] = c.real();
            dataPtr[2*i+1] = c.imag();
        }
    }
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Transform(const std::function<Element(const Element &)> &fn, const ExecutionPolicy &policy)
{
    switch(policy) {

        case ExecutionPolicy::Sequential:
            std::transform(std::execution::seq, this->begin(), this->end(), this->begin(), fn);
            break;
        case ExecutionPolicy::Unsequenced:
            std::transform(std::execution::unseq, this->begin(), this->end(), this->begin(), fn);
            break;
        case ExecutionPolicy::Parallel:
            std::transform(std::execution::par, this->begin(), this->end(), this->begin(), fn);
            break;
        case ExecutionPolicy::ParallelUnsequenced:
            std::transform(std::execution::par_unseq, this->begin(), this->end(), this->begin(), fn);
            break;

        default:
            std::transform(this->begin(), this->end(), this->begin(), fn);
            break;
    }

    return (*this);
}

template<typename Element>
TTensorT<Element>::~TTensorT() {

    this->Clear();
    
    if (pImpl) {
        delete pImpl;
        pImpl = nullptr;
    }
};

template<typename Element>
const char *TTensorT<Element>::Version() { return TTensorT<Element>::Impl::version(); }


template<typename Element>
TTensorT<Element>& TTensorT<Element>::Autograd(const bool requires_grad) 
{
    this->pImpl->requires_grad_(requires_grad);
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::To(TTensorDevice device) 
{
    this->pImpl->to(TTensorT<Element>::Impl::get_device(device));
    return (*this);
}

template<typename Element>
TTensorDevice TTensorT<Element>::GetDevice()
{
    const auto device = TTensorT<Element>::Impl::get_device();
    return TTensorT<Element>::Impl::get_device( device );
}

template<typename Element>
int TTensorT<Element>::GetDeviceCount()
{
    return TTensorT<Element>::Impl::device_count();
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Backward(const TTensorT<Element> &grad) 
{ 
    this->pImpl->backward(grad.GetImpl());
    return (*this);
}

template<typename Element>
void TTensorT<Element>::ManualSeed(const uint64_t &seed) 
{ 
    torch::manual_seed(seed);
    Helpers::SetRandomSeed(seed);
}

template<typename Element>
int64_t TTensorT<Element>::GetDataPtrOffset(int64_t index) const
{
    std::vector<int64_t> indices(this->GetDimensions(), 0);
    int64_t remainder = index;

    for (int i = this->GetDimensions() - 1; i >= 0; --i) {
        indices[i] = remainder / this->GetStride(i);
        remainder %= this->GetStride(i);
    }

    if (remainder != 0) {
        throw std::out_of_range("Index " + TString::Itoa(index,10) + " out of range in tensor " + GetShapeStr());
    }
    
    int64_t flatIndex = 0;
    for (int64_t i = 0; i < indices.size(); ++i) {
        flatIndex += indices[i] * this->GetStride(i);
    }

    return flatIndex;
}

template<typename Element>
Element *TTensorT<Element>::GetDataPtr(int64_t offset) const
{
    if(this->pImpl == nullptr) return nullptr;
    return reinterpret_cast<Element*>(this->pImpl->mutable_data_ptr()) + offset;
}

template<typename Element>
const Element& TTensorT<Element>::GetData(int64_t offset) const 
{
    Element *ptr = this->GetDataPtr(offset);
    if (ptr == nullptr) throw std::out_of_range("Data pointer for offset +"+TString::Itoa(offset,10)+" not found");

    return *ptr;
}

template<typename Element>
Element& TTensorT<Element>::GetData(int64_t index) 
{
    int64_t offset = this->GetDataIndex(this->GetDataIndices(index));
    Element *ptr = this->GetDataPtr(offset);
    if (ptr == nullptr) throw std::out_of_range("Data pointer for offset +"+TString::Itoa(offset,10)+" not found");

    return *ptr;
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::FFT_(int n, int64_t dim, const TTensorNorm &norm)
{
    (*this) = (Impl) torch::fft::fft(*this->pImpl, n, dim, this->pImpl->get_norm(norm).Data());
    return *this;
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::iFFT_(int n, int64_t dim, const TTensorNorm &norm)
{
    (*this) = (Impl) torch::fft::ifft(*this->pImpl, n, dim, this->pImpl->get_norm(norm).Data());
    return *this;
}

template<typename Element>
TTensorT<Element> &TTensorT<Element>::Stack_(int64_t dim, const std::vector<TTensorT<Element>> &tensorVector)
{
    std::vector<at::Tensor> tensorImplVector;
    tensorImplVector.push_back(this->GetImpl());
    for (const auto& tensor : tensorVector) {
        tensorImplVector.push_back(tensor.GetImpl());
    }

    (*this) = (Impl) torch::stack(at::TensorList(tensorImplVector), dim);
    return *this;
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Unfold_(int64_t dim, int64_t size, int64_t step)
{
    (*this) = (Impl) this->pImpl->unfold(dim, size, step);
    return *this;
}

template<typename Element>
bool TTensorT<Element>::IsContiguous () const { return this->pImpl->is_contiguous(); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Contiguous_()
{
    (*this) = (Impl) this->pImpl->contiguous();
    return *this;
}

template<typename Element>
void TTensorT<Element>::Clear() { this->pImpl->resize_(0); }
template<typename Element>
void TTensorT<Element>::Empty() { this->Fill_(0); }
template<typename Element>
TTensorT<Element> TTensorT<Element>::Clone() const
{
    TTensorT<Element> C = (Impl) this->pImpl->clone();
    return C;
}

template<typename Element>
TString TTensorT<Element>::GetShapeStr() const { return GetShapeStr(this->GetShape()); }
template<typename Element>
TString TTensorT<Element>::GetShapeStr(const std::vector<int64_t>& shape) const { return GetString(shape, "x", "", ""); }
template<typename Element>
TString TTensorT<Element>::GetStridesStr() const { return GetString(GetStrides()); }
template<typename Element>
TString TTensorT<Element>::GetStridesStr(const std::vector<int64_t>& strides) const { return GetString(strides); }
template<typename Element>
TString TTensorT<Element>::GetString(const std::vector<int64_t>& index, TString delimiter, TString leftBracket, TString rightBracket, bool fillZeros) const
{
    std::ostringstream oss;
    oss << leftBracket;

    if(index.size() < 1) { 
    
        if(fillZeros) oss << "0" << delimiter << "0";    
    
    } else if (index.size() < 2) {
        
        if(fillZeros) oss << index[0] << delimiter << "1";
        else oss << index[0];

    } else {

        for (int64_t i = 0; i < index.size(); ++i) {

            oss << index[i];
            if (i != index.size()-1) {
                oss << delimiter;
            }
        }
    }

    oss << rightBracket;
    return TString(oss.str().c_str());
}

template<typename Element>
Element TTensorT<Element>::Trace() const
{
    Element tr = 0;

    int64_t nDims = this->GetDimensions();
    if(nDims < 1) return tr;

    auto shape = this->GetShape();
    int min = std::min_element(std::execution::par_unseq, shape.begin(), shape.end()) - shape.begin();
    if (min == this->GetDimensions()) return tr;

    for(int64_t i = 0; i < shape[min]; i++) {
    
        std::vector<int64_t> diag(nDims, i);
        tr += this->GetData(diag);
    }

    return tr;
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Permute_(const std::vector<int64_t>& perms) 
{ 
    *(this->pImpl) = this->pImpl->permute(perms); 
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Transpose_(int64_t dim0, int64_t dim1) 
{ 
    this->pImpl->transpose_(dim0, dim1);
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Concatenate_(const TTensorT<Element>& B, int64_t dim) 
{ 
    *(this->pImpl) = torch::cat({this->GetImpl(), B.GetImpl()}, dim);
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Contraction_(const TTensorT<Element>& B) 
{ 
    *(this->pImpl) = torch::matmul(this->GetImpl(), B.GetImpl());
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Slice_(const std::vector<std::tuple4<int64_t>>& shape)
{
    int N =  this->GetDimensions();
    std::vector<torch::indexing::TensorIndex> slices( N, torch::indexing::Slice() );

    for (const auto& spec : shape) {

        int64_t dim = std::get<0>(spec);
        int64_t start = std::get<1>(spec);
        int64_t end = std::get<2>(spec);
        int64_t step = std::get<3>(spec);
        
        if(dim >= 0 && dim < N) {
            slices[dim] = torch::indexing::Slice(start, end, step);
        }
    }

    *(this->pImpl) = this->pImpl->index(slices);
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Cumsum_(int64_t dim) { this->pImpl->cumsum_(dim, Impl::dtype()); return (*this); }

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Sum_(int64_t dim, std::vector<T> weight) 
{
    T sum_of_weights = std::accumulate(weight.begin(), weight.end(), T(0));
    if (dim >= 0 && dim < this->GetDimensions()) {

        if (weight.size() < 1) {
        
            *(this->pImpl) = this->pImpl->sum(dim);

        } else {

            std::vector<int64_t> shape(this->pImpl->sizes().begin(), this->pImpl->sizes().end());
            shape[dim] = -1;  // Set the dimension to be reduced to -1

            torch::Tensor weight_tensor = TTensorT<T>::FromBlob(weight, shape);
            torch::Tensor weighted_tensor = (* this->pImpl) * weight_tensor;
            torch::Tensor sum_of_weights_tensor = TTensorT<T>::FromBlob({sum_of_weights}, shape);
            *(this->pImpl) = weighted_tensor.sum(dim) / sum_of_weights_tensor;
        }
    }

    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Mean_(int64_t dim, std::vector<T> weight) 
{
    T sum_of_weights = std::accumulate(weight.begin(), weight.end(), T(0));
    if (dim >= 0 && dim < this->GetDimensions()) {

        if (weight.size() < 1) {
        
            *(this->pImpl) = this->pImpl->mean(dim);

        } else {

            std::vector<int64_t> shape(this->pImpl->sizes().begin(), this->pImpl->sizes().end());
            shape[dim] = -1;  // Set the dimension to be reduced to -1

            torch::Tensor weight_tensor = TTensorT<T>::FromBlob(weight, shape);
            torch::Tensor weighted_tensor = (* this->pImpl) * weight_tensor;
            torch::Tensor sum_of_weights_tensor = TTensorT<T>::FromBlob({sum_of_weights}, shape);
            *(this->pImpl) = weighted_tensor.mean(dim) / sum_of_weights_tensor;
        }
    }

    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Min_()
{
    *(this->pImpl) = this->pImpl->min();
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Max_()
{
    *(this->pImpl) = this->pImpl->max();
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Inverse_() { 

    *(this->pImpl) = (Impl) this->pImpl->inverse();
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Pow_(const T& c) { this->pImpl->pow_(c); return (*this); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Pow_(const TTensorT<T>& B) { this->pImpl->pow_(B.GetImpl()); return (*this); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Abs_() { this->pImpl->abs_(); return (*this); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Exp_() { torch::exp_(*this->pImpl); return (*this); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Solve_(const TTensorT<Element> &B, bool Ax) 
{ 
    *(this->pImpl) = (Impl) torch::linalg_solve(*this->pImpl, B.GetImpl(), Ax);
    return (*this);
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Tile_(const std::vector<int64_t>& dims)
{ 
    *(this->pImpl) = (Impl) at::tile(*this->pImpl, dims);
    return *this;
}


template<typename Element>
TTensorT<Element>& TTensorT<Element>::Fill_(const Element& c) { return this->Map([&] () { return c; }); }

template<typename Element>
TTensorT<Element>& TTensorT<Element>::FillRandom_(const Element &max) { return this->FillRandom_(0, max); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::FillRandom_(const Element &min, const Element &max)
{ 
    return this->Map([&] (const int64_t& index) { 
        UNUSED(index);
        return min + ROOT::IOPlus::Helpers::GetRandom<Element>(max);
    });
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::FillRandomInt_(const int &max) { return this->FillRandomInt_(0, max); }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::FillRandomInt_(const int &min, const int &max) { return this->Map([&] () { return min + ROOT::IOPlus::Helpers::GetRandom<int>(max); }); }

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Squeeze_(const int64_t& dim) { this->pImpl->squeeze_(dim); return *this; }
template<typename Element>
TTensorT<Element>& TTensorT<Element>::Unsqueeze_(const int64_t& dim) { this->pImpl->unsqueeze_(dim); return *this; }

template<typename Element>
TTensorT<Element> TTensorT<Element>::Eye(const int64_t& n, const int64_t& m) { return TTensorT<Element>(torch::eye(n, m ? m : n, torch::dtype(TTensorT<Element>::Impl::dtype()))); }

template<typename Element>
TTensorT<Element> TTensorT<Element>::Arange(const int64_t& start, const int64_t& stop, const int64_t& step) { return TTensorT<Element>(torch::arange(start, stop, step, torch::dtype(TTensorT<Element>::Impl::dtype()))); }

template<typename Element>
TTensorT<Element> TTensorT<Element>::Linspace(const int64_t& start, const int64_t& stop, const int64_t& step) { return TTensorT<Element>(torch::linspace(start, stop, step, torch::dtype(TTensorT<Element>::Impl::dtype()))); }

template<typename Element>
TTensorT<Element> TTensorT<Element>::Logspace(const int64_t& start, const int64_t& stop, const int64_t& step, double base) { return TTensorT<Element>(torch::logspace(start, stop, step, base, torch::dtype(TTensorT<Element>::Impl::dtype()))); }

template<typename Element>
TTensorT<Element> TTensorT<Element>::Full(const Element &c, std::vector<int64_t> shape) 
{ 
    if constexpr (std::is_complex_v<Element>) {
        return TTensorT<Element>(torch::full(at::IntArrayRef(shape), c10::complex<T>(c), torch::dtype(TTensorT<Element>::Impl::dtype())));
    } else {
        return TTensorT<Element>(torch::full(at::IntArrayRef(shape), at::Scalar(c), torch::dtype(TTensorT<Element>::Impl::dtype())));
    }
}

template<typename Element>
TTensorT<Element> TTensorT<Element>::Rand(std::vector<int64_t> shape) { return TTensorT<Element>(torch::rand(at::IntArrayRef(shape), torch::dtype(TTensorT<Element>::Impl::dtype()))); }
template<typename Element>
TTensorT<Element> TTensorT<Element>::Randint(const int64_t &low, const int64_t &high, std::vector<int64_t> shape) { return TTensorT<Element>(torch::randint(low, high, at::IntArrayRef(shape), torch::dtype(TTensorT<Element>::Impl::dtype()))); }
template<typename Element>
TTensorT<Element> TTensorT<Element>::Randn(std::vector<int64_t> shape) { return TTensorT<Element>(torch::randn(at::IntArrayRef(shape), torch::dtype(TTensorT<Element>::Impl::dtype()))); }
template<typename Element>
TTensorT<Element> TTensorT<Element>::Randperm(const int64_t &n) { return TTensorT<Element>(torch::randperm(n, torch::dtype(TTensorT<Element>::Impl::dtype()))); }

template<typename Element>
bool TTensorT<Element>::InRange(const std::vector<int64_t>& indices) const
{
    if (indices.size() != this->GetDimensions()) return false;

    for (int64_t i = 0; i < indices.size(); ++i) {
        if (indices[i] >= this->GetEntries(i)) return false;
    }

    return true;
}


template<typename Element>
int64_t TTensorT<Element>::GetDimensions() const { return this->pImpl->dim(); }
template<typename Element>
std::vector<int64_t> TTensorT<Element>::GetShape() const
{
    if(this->pImpl == NULL) return std::vector<int64_t>();

    torch::IntArrayRef sizes = this->pImpl->sizes();
    return std::vector<int64_t>(sizes.begin(), sizes.end());
}

template<typename Element>
int64_t TTensorT<Element>::GetStride(int64_t dim) const
{
    if (dim != 0 && (dim < 0 || dim >= this->GetDimensions())) {
        throw std::out_of_range("Dimension `"+TString::Itoa(dim,10)+"` is out of bounds, this tensor has "+this->GetDimensions()+" dimension(s).");
    }

    return this->GetDimensions() == 0 ? 1 : GetStrides()[dim];    
}

template<typename Element>
std::vector<int64_t> TTensorT<Element>::GetStrides() const { 

    if(this->pImpl == NULL) return std::vector<int64_t>();
    if(!this->pImpl->defined()) return std::vector<int64_t>();

    auto strides = this->pImpl->strides();
    return std::vector<int64_t>(strides.begin(), strides.end());
}

template<typename Element>
int64_t TTensorT<Element>::GetEntries() const
{
    return this->pImpl != NULL ? this->pImpl->numel() : 0;
}

template<typename Element>
int64_t TTensorT<Element>::GetEntries(int64_t dim) const
{ 
    if (dim != 0 && (dim < 0 || dim >= this->GetDimensions())) {
        throw std::out_of_range("Invalid dimension (" + TString::Itoa(dim,10) + ") provided as compared to tensor shape " + this->GetShapeStr() + ".");
    }

    return this->pImpl->sizes()[dim];
}

template<typename Element>
TString TTensorT<Element>::GetDataIndicesStr(const std::vector<int64_t>& indices) const { return GetString(indices, ", ", "{", "}", false); }
template<typename Element>
int64_t TTensorT<Element>::GetDataIndex(const std::vector<int64_t>& indices) const
{
    if (indices.size() == 1 && indices[0] >= 0 && indices[0] < this->GetEntries()) return indices[0];

    std::vector<int64_t> strides = GetStrides(); 
    if (!InRange(indices)) {
        throw std::out_of_range("Index vector "+GetDataIndicesStr(indices)+" is out of range in tensor " + GetShapeStr());
    }

    int64_t flatIndex = 0;
    for (int64_t i = 0; i < strides.size(); ++i) {
        flatIndex += indices[i] * strides[i];
    }
    
    return flatIndex;
}

template<typename Element>
std::vector<int64_t> TTensorT<Element>::GetDataIndices(int64_t index) const
{
    std::vector<int64_t> indices(this->GetDimensions(), 0);
    int64_t remainder = index;

    for (int i = this->GetDimensions() - 1; i >= 0; --i) {
        indices[i] = remainder % this->GetEntries(i);
        remainder /= this->GetEntries(i);
    }

    if (remainder > 0 || !InRange(indices)) {    
        throw std::out_of_range("Index " + TString::Itoa(index, 10) + " (" +this->GetDataIndicesStr(indices)+ ") out of range in tensor " + GetShapeStr());
    }

    return indices;
}

template<typename Element>
int64_t TTensorT<Element>::GetDataIndex(const Element* ptr) const
{
    Iterator iter = begin();
    int64_t index = 0;

    // Iterate through the container
    while (iter != end()) {

        if (iter.operator->() == ptr) { // Compare pointers
            return index; // Return index if found
        }

        ++iter;
        ++index;
    }

    // If ptr is not found, return a value indicating not found (-1 or similar)
    return static_cast<size_t>(-1); // Change this according to your needs
}

template<typename Element>
bool TTensorT<Element>::IsComplex() {

    if constexpr (std::is_same_v<Element, std::complex<float>>)
        return true;
    if constexpr (std::is_same_v<Element, std::complex<double>>)
        return true;

    return false;
}

template<typename Element>
void TTensorT<Element>::Print(Option_t* option) const
{
    TString opt = option;
    int64_t nSize = GetEntries();

    std::cout << "TTensorT<" << TPrint::GetType<Element>() << ">(" << this << ").Print";
    std::cout << " Shape= " << std::setw(5) << GetShapeStr() << ",";
    std::cout << " Strides= " << std::setw(5) << GetStridesStr() << ",";
    std::cout << " Entries=" << std::setw(5) << nSize << ",";
    std::cout << " Total size= " << (nSize * sizeof(Element)) << "B" << std::endl;

    if((opt.Contains("all") || opt.Contains("*"))) {
        std::cout << std::setw(std::cout.precision()+5);
        std::cout << (*this) << std::endl;
    }
}

template<typename Element>
int TTensorT<Element>::PrintHelper(std::ostream& os, std::vector<int64_t> indices) const
{
    int dim = indices.size();

    int w0 = os.width();
    int w = TMath::Max(w0, (int) os.precision() + (std::is_floating<Element>() ? 5 : 0));
    if(dim == 0) os.width(std::offset());

    int64_t nDim = this->GetEntries(dim);
    int64_t nLimit = std::limit();
    if (nDim - nLimit < 3) nLimit = nDim;

    int depth = 0;

    os << "[";
    if (this->GetDimensions() == 0) os << this->GetData(0);
    else if (dim == this->GetDimensions() - 1) {

        for (size_t i = 0; i < this->GetEntries(dim); ++i) {

            if (i > nLimit/2 && i < nDim-nLimit/2) continue;
            if (i == nLimit/2 && nLimit != nDim) {
                os << std::delimiter();
                os.width(w-dim);
                os << " ..,";
                continue;
            }

            if (i > 0) {
                if(i != nDim-nLimit/2) os << std::delimiter();
                os << " ";
            }

            std::vector<int64_t> _indices = indices;
                                 _indices.push_back(i);

            os << std::setw(w-dim);
            os << this->GetData(_indices);
        }

    } else {

        for (size_t i = 0; i < this->GetEntries(dim); ++i) {

            std::vector<int64_t> _indices = indices;
                                 _indices.push_back(i);

            std::setw(w0);
            depth = PrintHelper(os, _indices) + 1;

            if (i != this->GetEntries(dim) - 1) {

                os.width(std::offset());
                for(int j = 0; j < depth; j++)
                    os << std::endl;
                
                for (size_t j = 0; j <= dim; ++j)
                    os << " ";
            }
        }
    }

    std::cout << "]" << std::flush;
    return depth;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator=(const TTensorT<Element>& B) 
{
    return this->operator=(B.GetImpl());
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator=(const TTensorT<Element>::Impl& impl) 
{
    *(this->pImpl) = impl;
    return *this;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator=(const Element& c) 
{ 
    return this->Fill_(c);
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator^=(const T& e) { return this->Pow_(e); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator^ (const T& e) const
{
    TTensorT<Element> A  = this->Clone();
                      A ^= e;
    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator^=(const TTensorT<T>& B) { return this->Pow_(B); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator^ (const TTensorT<T>& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A ^= B;
    return A;
}

template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator~ () const { return this->Inverse(); }

template<typename Element> 
bool TTensorT<Element>::operator==(const TTensorT<Element>& B) const 
{     
    return this->GetShape() == B.GetShape() && std::equal(std::execution::par, this->begin(), this->end(), B.begin());    
}

template<typename Element>
TTensorT<Element>& TTensorT<Element>::Detach_()
{
    (*this) = (Impl) torch::detach(this->GetImpl());
    return (*this);
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator+=(const TTensorT& B) { this->pImpl->add_(B.GetImpl()); return (*this); }
template<typename Element> 
TTensorT<Element>  TTensorT<Element>::operator+ (const TTensorT& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A += B;
    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator-=(const TTensorT& B) { this->pImpl->sub_(B.GetImpl()); return (*this); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator- (const TTensorT& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A -= B;
    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator*=(const TTensorT<Element>& B) { this->pImpl->mul_(B.GetImpl()); return (*this); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator* (const TTensorT<Element>& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A *= B;

    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator/=(const TTensorT& B) { this->pImpl->div_(B.GetImpl()); return (*this); }
template<typename Element> 
TTensorT<Element>  TTensorT<Element>::operator/ (const TTensorT& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A /= B;
    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator|=(const TTensorT<Element>& B) { return this->Concatenate_(B, this->GetDimensions()-1); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator| (const TTensorT<Element>& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A |= B;

    return A;
}

template<typename Element> 
TTensorT<Element>& TTensorT<Element>::operator%=(const TTensorT<Element>& B) { this->Contraction_(B); return (*this); }
template<typename Element> 
TTensorT<Element> TTensorT<Element>::operator% (const TTensorT<Element>& B) const
{
    TTensorT<Element> A  = this->Clone();
                      A %= B;

    return A;
}

}} // namespace ROOT::IOPlus

/// \cond NOPE
template class ROOT::IOPlus::TTensorT<int>;
template class ROOT::IOPlus::TTensorT<short>;
template class ROOT::IOPlus::TTensorT<float>;
template class ROOT::IOPlus::TTensorT<std::complex<float>>;
template class ROOT::IOPlus::TTensorT<double>;
template class ROOT::IOPlus::TTensorT<std::complex<double>>;
/// \endcond
