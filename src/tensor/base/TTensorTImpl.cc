#include "ROOT/IOPlus/Impl/TTensorTImpl.h"

namespace ROOT { namespace IOPlus {

    template<typename Element>
    TTensorT<Element>::Impl::Impl(const at::IntArrayRef &sizes, TTensorDevice device, TTensorLayout layout, const bool &requires_grad) 
        : torch::Tensor(torch::zeros(sizes, 
          torch::TensorOptions().dtype(dtype())
                                .layout(get_layout(layout))
                                .requires_grad(requires_grad))) { this->to(this->get_device(device)); }

    template<typename Element>
    TTensorT<Element>::Impl::Impl(const Impl& impl) : Impl((torch::Tensor) impl) { }

    template<typename Element>
    TTensorT<Element>::Impl::Impl(const torch::Tensor& tensor) : torch::Tensor(tensor) { }

    template<typename Element>
    const char *TTensorT<Element>::Impl::version() { return TORCH_VERSION; }

    template<typename Element>
    int TTensorT<Element>::Impl::device_count() {

        if(torch::cuda::is_available())
            return torch::cuda::device_count();
        if(torch::mps::is_available() && !std::is_same_v<Element, double> && !std::is_same_v<T, double>)
            return 1; // lack of support for MPS device_count() in libtorch (@TODO: check if this is still the case later)

        return 0;
    }

    template<typename Element>
    typename TTensorT<Element>::Impl TTensorT<Element>::Impl::from_blob(const std::vector<Element>& data, torch::IntArrayRef sizes) {
        return torch::from_blob((void *) data.data(), sizes, Impl::dtype());
    }

    template<typename Element>
    torch::Device TTensorT<Element>::Impl::get_device(TTensorDevice device) {
        
        switch(device) {

            case TTensorDevice::Auto:
                try { return get_device(TTensorDevice::GPU); }
                catch (std::invalid_argument const& ex) { return get_device(TTensorDevice::CPU); }
                
            case TTensorDevice::GPU:
                if(torch::cuda::is_available() && torch::cuda::device_count() > 0)
                    return torch::kCUDA;
                if(torch::mps::is_available() && !std::is_same_v<Element, double> && !std::is_same_v<T, double>)
                    return torch::kMPS;

                throw std::invalid_argument("No supported GPU is available on this machine. (supported: CUDA, Metal Performance Shaders)");

            default: [[fallthrough]];
            case TTensorDevice::CPU:
                return torch::kCPU;
        }
    }

    template<typename Element>
    TTensorDevice TTensorT<Element>::Impl::get_device(torch::Device device) {
        
        if ( device == torch::kCUDA || device == torch::kMPS )
            return TTensorDevice::GPU;

        if ( device == torch::kCPU )
            return TTensorDevice::CPU;

        throw std::invalid_argument("Unsupported device type for torch tensor.");

    }

    template<typename Element>
    torch::Layout TTensorT<Element>::Impl::get_layout(TTensorLayout layout) {
        
        switch(layout) {

            default: [[fallthrough]];
            case TTensorLayout::Stride:
                return torch::kStrided;
            case TTensorLayout::Sparse:
                return torch::kSparse;
        }
    }

    // Function template to determine torch::ScalarType
    template<typename Element>
    torch::ScalarType TTensorT<Element>::Impl::dtype() {
        if constexpr (std::is_same_v<Element, char>)
            return torch::kInt8;
        if constexpr (std::is_same_v<Element, short>)
            return torch::kInt16;
        if constexpr (std::is_same_v<Element, int>)
            return torch::kInt32;
        if constexpr (std::is_same_v<Element, long>)
            return torch::kInt64;
        if constexpr (std::is_same_v<Element, float>)
            return torch::kFloat32;
        if constexpr (std::is_same_v<Element, std::complex<float>>)
            return torch::kComplexFloat;
        if constexpr (std::is_same_v<Element, double>)
            return torch::kFloat64;
        if constexpr (std::is_same_v<Element, std::complex<double>>)
            return torch::kComplexDouble;

        throw std::invalid_argument("Unsupported data type for torch tensor.");
    }

    template<typename Element>
    TString TTensorT<Element>::Impl::get_norm(const TTensorNorm& norm) {

        if(norm == TTensorNorm::Backward)
            return "backward";
        if(norm == TTensorNorm::Ortho)
            return "ortho";
        if(norm == TTensorNorm::Forward)
            return "forward";
        
        throw std::invalid_argument("Unsupported FFT normalization provided.");
    }

    template<typename Element>
    typename TTensorT<Element>::Impl& TTensorT<Element>::Impl::operator=(const Impl& other) {
        
        if (this != &other) torch::Tensor::operator=(other);
        return *this;
    }
}}

template class ROOT::IOPlus::TTensorT<int>::Impl;
template class ROOT::IOPlus::TTensorT<short>::Impl;
template class ROOT::IOPlus::TTensorT<float>::Impl;
template class ROOT::IOPlus::TTensorT<std::complex<float>>::Impl;
template class ROOT::IOPlus::TTensorT<double>::Impl;
template class ROOT::IOPlus::TTensorT<std::complex<double>>::Impl;
