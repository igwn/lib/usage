/**
 * @file TFileReader.h
 * @brief Header of the TFileReader class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TFileReader
 * @brief Description of the TFileReader class.
 * @details Detailed description of the TFileReader class, its functionality, and usage.
 */

#pragma once

#include <Riostream.h>
#include <TPDF.h>

#include <algorithm>
#include <TROOT.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <TUrl.h>
#include <TKey.h>
#include <TFile.h>
#include <TSystem.h>
#include <TDirectory.h>
#include <TObjString.h>
#include <TRegexp.h>
#include <TRandom3.h>
#include <TChain.h>
#include <vector>
#include <fstream>
#include <TROOT.h>
#include <TCanvas.h>

#include <TProfile.h>
#include <TProfile2D.h>

#include <TGraph.h>
#include <TGraph2D.h>

#include <TH3.h>
#include <TH2.h>
#include <TH1.h>

#include <TPrint.h>
#include <TObjString.h>

#include <functional>
#include <dirent.h>

#if __cplusplus >= 201703L // (C++17)
#include <filesystem>
namespace fs = std::filesystem;
#elif __cplusplus >= 201402L // C++14
#include <experimental/filesystem>     
namespace fs = std::experimental::filesystem;
#else
#include <sys/types.h>
#include <sys/stat.h>
#endif

class TFileReader: public TNamed
{
        private:
        TString origin;
        TString option;

        std::vector<TString> vTypes;
        std::vector<TString> vFqfns;
        std::vector<TString> vClassNames;

        int maxDepth;
        TString methodName;
        TString className;

        public:

        static int Instance;
        static std::vector<TFile*> vFiles;

        // Load method..
        static const int kList;
        static const int kChain1D;
        static const int kChain2D;
        static const int kSum;
        static const int kTree;
        
        //For file re-opening
        static const int kOpenLimit;
        static bool kOpenLimitTriggered;

        static const bool kReopen;
        static const bool kReuse;
        
        static const char kSeparator;
        
        static const bool kUniqID;
        static const bool kAllID;
        
        static const int kDefaultDepth;
        static const int kDefaultKey;
        
        TFileReader() { TFileReader::Instance++; };

        TFileReader(const char* name, const char* option, TClass* cl, int maxDepth = TFileReader::kDefaultDepth)
                : TFileReader(name, option, "", (cl == NULL) ? TString("") : TString(cl->GetName()), maxDepth) {
        }

        TFileReader(const char* name, const char* option, const char* origin, TClass* cl, int maxDepth = TFileReader::kDefaultDepth);

        TFileReader(const char* name, const char* option, const char* origin = "",
                        TString className = "",
                        int maxDepth = TFileReader::kDefaultDepth)
        {
                this->SetName(name);
                this->SetTitle("TFileReader(\"" + TString(name) + "\", \""+option+"\")");
                this->className = className;
                this->option = option;
                this->maxDepth = maxDepth;

                if(!TString(origin).EqualTo("")) this->Init(origin);
                TFileReader::Instance++;
        }

        ~TFileReader()
        {
                TFileReader::Instance--;
                this->Clear();
        }

        //
        // Standard prototypes
        //
        static bool Touch(TString);
        static bool IsEmpty(TString);
        static bool CleanTextFile(TString);

        bool Find(TString);
        bool IsValid();

        void SetClassName(TClass *cl)
        {
                className = (cl == NULL) ? "" : TString(cl->GetName());
        }

        inline TString GetOrigin() { return this->origin; }
        bool Init(TString="");
        bool Load(TString);

        static TString FetchTreeDescriptor(TString);

        inline int GetN() { return this->vFqfns.size(); }

        inline std::vector<TString> GetPaths() {

                std::vector<TString> vPath;
                for(int i = 0, N = this->vFqfns.size(); i < N; i++)
                        vPath.push_back(TFileReader::Obj(TFileReader::ObjDirName(this->vFqfns[i])));

                return vPath;
        };

        static inline bool AppendToDirectory(std::vector<TObject*> v,     TString dir) { return AppendToDirectory(v, gDirectory->mkdir(dir)); }
        static        bool AppendToDirectory(std::vector<TObject*> v, TDirectory *dir);
        static inline bool AppendToDirectory(TObject* obj,     TString dir) { return AppendToDirectory(std::vector<TObject*>{obj}, gDirectory->mkdir(dir)); }
        static inline bool AppendToDirectory(TObject* obj, TDirectory *dir) { return AppendToDirectory(std::vector<TObject*>{obj}, dir); }
        static inline bool AppendToDirectory(TList *l,     TString dir) { return AppendToDirectory(l, gDirectory->mkdir(dir)); }
        static        bool AppendToDirectory(TList *l, TDirectory *dir);

        static TString RelativePath(TString, TString = gSystem->WorkingDirectory());

        static std::vector<TObjString*> ReadFileObjString(TString);
        static std::vector<TString> ReadFile(TString);

        std::vector<TKey*> GetKeys();
        
        inline TObject* GetObject(int i) { return this->GetObjects()[i]; }
        std::vector<TObject*> GetObjects();

        template <typename T, typename Ti>
        static inline void Reorder(std::vector<T>& v, const std::vector<Ti>& indexes) {

                assert(v.size() == indexes.size());

                // Create a temporary vector to hold the reordered elements
                std::vector<T> temp(v.size());
                for(size_t i = 0; i < indexes.size(); ++i) {

                        assert(indexes[i] < v.size()); // Ensure the index is within bounds
                        temp[i] = v[indexes[i]]; // Place each element at its new position
                }

                v = std::move(temp); // Efficiently assign the temporary vector to v
        }

        inline std::vector<TString> GetFqfn(TString name = "") {

                std::vector<TString> v;
                if(name.EqualTo("")) v = this->vFqfns;
                else {
                        for(int i = 0, N = vFqfns.size(); i < N; i++)
                                if(vFqfns[i].Contains(name)) v.push_back(vFqfns[i]);
                }
                return v;
        };

        inline std::vector<TString> GetTypes(TString name = "") {

                std::vector<TString> v;
                if(name.EqualTo("")) v = this->vTypes;
                else {
                        for(int i = 0, N = vFqfns.size(); i < N; i++)
                                if(vFqfns[i].Contains(name)) v.push_back(vTypes[i]);
                }
                return v;
        };

        inline std::vector<TString> GetClassNames(TString name = "") {

                std::vector<TString> v;
                if(name.EqualTo("")) v = this->vClassNames;
                else {

                        for(int i = 0, N = vFqfns.size(); i < N; i++)
                                if(vFqfns[i].Contains(name)) v.push_back(vClassNames[i]);
                }
                return v;
        };

        static std::vector<TString> Browse(TDirectory * = NULL);

        inline Option_t* GetOption() { return this->option; }

        static TKey* GetKey(TString, TString = "");
        static TKey* GetKey(TDirectory *dir, TString obj_cmpl);

        static bool UnlinkEmptyCorrupt(TString);

        bool Add(int);
        bool Add(TString);
        bool Add(std::vector<TString>);
        bool Add(std::vector<std::vector<TString>>);

        void Print();
        inline bool Isset(unsigned int i = 0, TString obj = "") { return TFileReader::Isset(this->At(i), obj); }
        static bool Isset(TString, TString = "");

        static   TList* ReadObj(TList*);
        static TObject* ReadObj(TKey*);
        static TObject* ReadObj(TString, TString = "");

        static TObject* ReadObj(TDirectory *dir, TString obj_cmpl);

        void Clear();

        //
        // Generic methods
        static std::vector<TString> LsRoot(TString, Option_t* = "", int = -1);
        static std::vector<TString> LsRoot(TDirectory*, Option_t* = "fdc", int = -1, TList* = NULL);

        static bool HasPattern(TString);

        static std::vector<TString> ParseIdentifier(TString, TString = (TString) TFileReader::kSeparator+"{}");
        inline std::vector<TString> ParseIdentifier(unsigned int i, TString pattern = (TString) TFileReader::kSeparator+"{}") { return TFileReader::ParseIdentifier(this->At(i), pattern); }

        inline std::vector<std::vector<TString>> ParseIdentifiers(TString pattern = (TString) TFileReader::kSeparator+"{}")
        {
                std::vector<std::vector<TString>> identifiers;
                for(int i = 0, N = this->vFqfns.size(); i < N; i++) {

                        std::vector<TString> identifier = this->ParseIdentifier(i, pattern);
                        if(identifier.size() == 0) continue;

                        identifiers.push_back(identifier);
                }

                return identifiers;
        }

        inline TString SubstituteIdentifier(TString pattern, unsigned int i) { return TFileReader::SubstituteIdentifier(pattern, this->At(i)); }
        static TString SubstituteIdentifier(TString pattern, TString identifier);
        static TString SubstituteIdentifier(TString pattern, std::vector<TString> identifier);

        static void ParseFile(TString, TString&, TString&, TString&);
        static std::vector<TString> ExpandWildcard(TString, Option_t* = "fdc", int = -1); // f=file; d=directory; c=cycle; t=textfile (look inside)
        static std::vector<TString> ExpandWildcardUrl(std::vector<TString>, Option_t* = "fdc");
        static std::vector<TString> ExpandWildcardUrl(TString, Option_t* = "fdc");
        static std::vector<TString> ExpandWildcardObj(TString, TString, Option_t* = "fdc", int = TFileReader::kDefaultDepth);
        static std::vector<TString> SplitString(const TString, TString = " ");

        static bool IsProperlyFormatted(TString);
        static bool IsRootFile(TString);
        static bool IsXmlFile(TString);
        static bool IsDirectory(TString, TString = "");
        static bool IsTextFile(TString);
        
        static bool Exists(TString, TString = "");
        
        static std::vector<TString> ExpandTextFile(std::vector<TString>, Option_t* = "fdc", int = TFileReader::kDefaultDepth);
        static std::vector<TString> RecursiveExpandTextFile(TString);
        static std::vector<TString> ListDirectory(TString dir = ".", bool bRecursive = false);
        static bool WritePDF(TString, Option_t *, TList * = NULL);
        static bool WritePDF(TString, Option_t *, TString);

        TString GetMethodName(TString);
        static TString ObjBaseName(TString);
        static TString ObjDirName(TString, int = -1);
        static int ObjCycle(TString);
        static int GetLastCycle(TString, TList*);

        static TString ObjRelativeDirName(TString);
        static TString KeyPath(TKey *);
        static TString BaseName(TString, TString = "");
        static TString DirName(TString, int = -1);
        static TString Url(TString);
        static TString Domain(TString);
        static TString FileName(TString);
        static TString Obj(TString);

        static TString Extension(TString);
        static TString StripExtension(TString);
        static TString ExpandVariables(TString str, bool bPwd = false);

        inline bool IsProperlyFormatted() { return TFileReader::IsProperlyFormatted(this->origin);}
        inline bool IsRootFile() { return TFileReader::IsRootFile(this->origin);}
        inline bool IsRootDirectory(TString path) { return TFileReader::IsDirectory(this->origin, path); }
        inline bool IsTextFile() { return TFileReader::IsTextFile(this->origin);}

        inline bool HasOrigin() { return !this->origin.EqualTo("");}
        inline bool IsDefined() { return this->vFqfns.size(); }

        inline bool HasPattern() { return TFileReader::HasPattern(this->origin); }

        inline TString First() { return (this->vFqfns.size()) ? this->vFqfns[0] : ""; }
        inline TString Last() { return (this->vFqfns.size()) ? this->vFqfns[this->vFqfns.size()-1] : ""; }
        inline TString At(int i) { return (i < (int) this->vFqfns.size() && i >= 0) ? this->vFqfns[i] : ""; }

        inline TString fqfn(unsigned int i = 0) { return this->At(i); }
        inline TString BaseName(unsigned int i = 0) { return TFileReader::BaseName(this->At(i)); }
        inline TString DirName(unsigned int i = 0, int depth = -1) { return TFileReader::DirName(this->At(i), depth); }
        inline TString ObjBaseName(unsigned int i = 0) { return TFileReader::BaseName(this->At(i)); }
        inline TString ObjDirName(unsigned int i = 0, int depth = -1) { return TFileReader::ObjDirName(this->At(i), depth); }

        inline TString Url(unsigned int i = 0) { return TFileReader::Url(this->At(i));}
        inline TString Domain(unsigned int i = 0) { return TFileReader::Domain(this->At(i));}
        inline TString FileName(unsigned int i = 0) { return TFileReader::FileName(this->At(i));}
        inline TString Obj(unsigned int i = 0) { return TFileReader::Obj(this->At(i));}

        inline TKey* GetKey(unsigned int i = 0) { return TFileReader::GetKey(this->vFqfns[i]);}
        inline TObject* ReadObj(unsigned int i = 0) { return ReadObj(GetKey(i)); };

        inline TObject* Get(unsigned int i = 0) { return TFileReader::ReadObj(this->GetKey(i));}

        inline int GetNFiles() { return this->vFqfns.size();}

        static bool Open(TString, TFile*&, Option_t * = "", bool = TFileReader::kReopen);
        static bool Close(TFile* &f);
        static bool Close(int);

        static TString ApplyCorrection(TString);
        static TString ApplyCorrectionUrl(TString);
        static TString ApplyCorrectionObj(TString);

        static TObject *OpenAndRead(TString, TFile*&);
        
        static bool Write(TObject*, TString, Option_t * = "UPDATE");
        static bool CopyDirectory(TDirectory*, TDirectory*);

        static bool CopyTo(TDirectory*, TDirectory*);
        static bool CopyTo(std::vector<TObject*>, std::vector<TString>, TDirectory*);
        static bool CopyTo(std::vector<TString>, TDirectory*);
        static std::vector<TString> LsPath(TDirectory*);
        static std::vector<TObject*> LsDirectory(TDirectory* dir);
        static std::vector<TObject*> Get(std::vector<TObject*>, TString);
        static bool RecursiveRmEmptyDir(TDirectory*);
        static TDirectory* RecursiveMkdir(TString, TString, Option_t *opt);
        static TDirectory* RecursiveMkdir(TString, TDirectory* = NULL);

        TObject* MergeObjects(TString path, Option_t *opt = "", int buffer = 256);
        static TObject* MergeObjects(std::vector<TObject *> objects, Option_t *opt = "", int buffer = 256);
        static TObject* MergeObjects(TList *objects, Option_t *opt = "", int buffer = 256);
        std::vector<TObject*> MergeObjects(Option_t *opt = "", int buffer = 256);

        TObject* ChainObjects(TString path, Option_t *opt = "", int buffer = 256);
        static TObject* ChainObjects(std::vector<TObject *> objects, Option_t *opt = "", int buffer = 256);
        static TObject* ChainObjects(TList *objects, Option_t *opt = "", int buffer = 256);
        std::vector<TObject*> ChainObjects(Option_t *opt = "", int buffer = 256);

        ClassDef(TFileReader,1);
};