/**
 * @file TKeyboard.h
 * @brief Header of the TKeyboard class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TKeyboard
 * @brief Description of the TKeyboard class.
 * @details Detailed description of the TKeyboard class, its functionality, and usage.
 */

#pragma once

#include <Riostream.h>
#include <sys/stat.h>
#include <map>
#include <sstream>
#include <vector>

#include <TSystem.h>
#include <TPad.h>

#include <TObject.h>
#include <TROOT.h>
#include <unistd.h>
#include <sstream>
#include <sys/ioctl.h>
#include <TQObject.h>

#include "TPrint.h"
#include "ROOT/IOPlus/Helpers.h"

#ifdef _WIN32
#include <conio.h>
#else
#include <unistd.h>
#include <termios.h>
#endif

class TKeyboard {
public:
        public:

                // Deleted copy constructor and assignment operator to prevent copies
                TKeyboard(const TKeyboard&) = delete;
                TKeyboard& operator=(const TKeyboard&) = delete;

                // Static method to get the instance of the singleton
                static TKeyboard& Instance() {
                        static TKeyboard instance;  // This will be the only instance
                        return instance;
                }

                static const int DEFAULT_TICK_MS = 50;

                enum   KeyAction { Up, Down, Press };
                struct KeyEvent {

                        std::vector<unsigned char> keys;
                        KeyEvent(std::vector<unsigned char> keys = {}) : keys(keys) {}

                        bool operator!=(const KeyEvent& other) const { return !(*this == other); }
                        bool operator==(const KeyEvent& other) const 
                        {
                                if(other.keys.size() != this->keys.size()) return false;
                                
                                for(auto key : other.keys)
                                        if(!this->Contains(key)) return false;
                                for(auto key : keys)
                                        if(!other.Contains(key)) return false;

                                return true;
                        }

                        bool operator<(const KeyEvent& other) const { return keys < other.keys; }
                        bool operator>(const KeyEvent& other) const { return keys > other.keys; }

                        bool operator==(const std::vector<unsigned char>& other) const { return keys == other; }
                        bool operator!=(const std::vector<unsigned char>& other) const { return !(*this == other); }

                        friend std::ostream& operator<<(std::ostream& os, const KeyEvent& event) {

                                os << event.Reveal();
                                return os;
                        }

                        TString ToString() const
                        {
                                TString str;
                                for (size_t i = 0; i < this->keys.size(); ++i) {
                                        if(i) str += " ";
                                        str += TString(static_cast<unsigned char>(this->keys[i]));
                                }

                                return str;
                        }

                        std::vector<unsigned char> GetSequence() const { return keys; }

                        TString ToHex() const
                        {
                                TString str;
                                for (size_t i = 0; i < this->keys.size(); ++i) {
                                        if(i) str += " ";
                                        str += TKeyboard::Hex(static_cast<unsigned char>(this->keys[i]));
                                }

                                return str;
                        }

                        TString ToBinary() const
                        {
                                TString str;
                                for (size_t i = 0; i < this->keys.size(); ++i) {
                                        if(i) str += " ";
                                        str += TKeyboard::Binary(static_cast<unsigned char>(this->keys[i]));
                                }

                                return str;
                        }

                        TString ToDec() const
                        {
                                TString str;
                                for (size_t i = 0; i < this->keys.size(); ++i) {
                                        if(i) str += " ";
                                        str += TString::Itoa(TKeyboard::Dec(static_cast<unsigned char>(this->keys[i])), 10);
                                }

                                return str;
                        }

                        TString Reveal() const
                        {
                                TString str;
                                for (size_t i = 0; i < this->keys.size(); ++i) {
                                        if(i) str += " ";
                                        str += TKeyboard::Reveal(static_cast<unsigned char>(this->keys[i]));
                                }

                                return str;
                        }

                        bool Empty() const { return keys.empty(); }
                        bool Contains(const KeyEvent& other) const 
                        {
                                for(auto k : other)
                                        if(this->Contains(k)) return true;

                                return false;
                        }

                        bool Contains(const unsigned char &chr) const 
                        {
                                return std::find(keys.begin(), keys.end(), chr) != keys.end();
                        }

                        bool Contains(const std::vector<unsigned char> &chr) const 
                        {
                                for(auto c : chr)
                                        if(this->Contains(c)) return true;

                                return false;
                        }

                        // Type aliases for convenience
                        using iterator = std::vector<unsigned char>::iterator;
                        using const_iterator = std::vector<unsigned char>::const_iterator;

                        // Unified Iterator template
                        template <typename Iter>
                        class IteratorT {
                                public:
                                using iterator_category = std::input_iterator_tag;
                                using value_type = typename Iter::value_type;
                                using difference_type = std::ptrdiff_t;
                                using pointer = value_type*;
                                using reference = value_type&;
    
                                public:
                                // Constructor taking a generic iterator
                                IteratorT(Iter it) : it_(it) {}

                                // Dereference operator
                                typename Iter::reference operator*() const { return *it_; }

                                // Pre-increment operator
                                IteratorT& operator++() { ++it_; return *this; }

                                // Equality operator
                                bool operator==(const IteratorT& other) const { return it_ == other.it_; }

                                // Inequality operator
                                bool operator!=(const IteratorT& other) const { return it_ != other.it_; }

                        private:
                                Iter it_;
                        };

                        using Iterator = IteratorT<iterator>;
                        using ConstIterator = IteratorT<const_iterator>;

                        // Non-const begin function
                        Iterator begin() { return Iterator(keys.begin()); }

                        // Const-qualified begin function
                        ConstIterator begin() const { return ConstIterator(keys.cbegin()); }

                        // Non-const end function
                        Iterator end() { return Iterator(keys.end()); }

                        // Const-qualified end function
                        ConstIterator end() const { return ConstIterator(keys.cend()); }
                };

        protected:

                TKeyboard(): keylistUpdate(std::chrono::steady_clock::now()) { }


                int Read(bool wait = false);
                std::vector<unsigned char> ReadSequence(int tick = DEFAULT_TICK_MS);

                static int x,y;
                static const std::map<unsigned char, TString> asciiSpecMap;

		std::vector<KeyEvent> keylist, keylistFromGUI;
                std::chrono::steady_clock::time_point keylistUpdate;
                bool AllowPop(int tick) {

                        if(!keylist.size()) return false;

                        auto now = std::chrono::steady_clock::now();
                        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - keylistUpdate).count();
                        if (elapsed >= 10 * tick) {
                                keylistUpdate = now;
                                return true;
                        }
                        
                        return false;
                }
        public:

                ~TKeyboard() { }

                void Connect(TPad *c);
                static void Capture(Int_t event, Int_t x, Int_t y, TObject *selected);

                KeyEvent PressAnyToContinue();

                KeyEvent Listen(const std::vector<KeyEvent> &keyList = {}, KeyAction action = KeyAction::Press, int tick = DEFAULT_TICK_MS); // Useful for debugging
                void Dump(KeyAction action = KeyAction::Press, int tick = DEFAULT_TICK_MS);

                KeyEvent KeyPress(int tick = DEFAULT_TICK_MS);
                KeyEvent KeyUp(int tick = DEFAULT_TICK_MS);
                KeyEvent KeyDown(int tick = DEFAULT_TICK_MS);

                static TString Binary(unsigned char c);
                static TString Hex(unsigned char c);
                static TString Reveal(unsigned char c);
                static int Dec(unsigned char c);
};

R__EXTERN TKeyboard *gKeyboard;