#pragma once

#include <TROOT.h>
#include <TFile.h>
#include <fstream>
#include <sys/stat.h>
#include <thread>

#include "TCurlRequest.h"
#include "TCurlResponse.h"

class TCurlFile : public TFile {

    friend class TCurl;
    private:
        std::thread::id owner;

        class Impl;
        Impl *pImpl = NULL;

    protected:
        int ttl = 0;
        const char *fPool;
        mutable char *fCache = nullptr;
        mutable char *fChecksum = nullptr;

        TString GetCache(const char *uri) const; 
        const char *TempDirectory();
       
    public:

        TCurlFile(const char *fname, Option_t *option = "", int ttl = 0, const char *pool = "default", Int_t compression = ROOT::RCompressionSetting::EDefaults::kUseCompiledDefault);
        ~TCurlFile();

        inline static bool IsRemote(const char *uri) { return !IsLocal(uri); }
               static bool IsLocal(const char *uri);

        // HTTP header information
        TCurlDict GetHeaders() const;
        TString GetHeader(TString name) const;
        void ClearHeaders();

        TCurlFile &SetHeaders(const TCurlDict &);
        TCurlFile &AddHeader(TString header, TString value);
        TCurlFile &RemoveHeader(TString header);
        
        // Send HTTP request
        bool Hit (std::shared_ptr<TCurlRequest> request = NULL, Option_t *option = "");
        bool Send(std::shared_ptr<TCurlRequest> request, Option_t *option = "");
        std::shared_ptr<const TCurlResponse> GetResponse() const;
        std::shared_ptr<const TCurlRequest> GetRequest() const;

        inline bool Fetch   (const char *uri, Option_t *option = "", const TCurlDict &headers = {}) { return GET(uri, option, headers); }
        inline bool Download(const char *uri, Option_t *option = "", const TCurlDict &headers = {}) { return GET(uri, option, headers); }

        inline bool GET   (const char *uri, Option_t *option = "", const TCurlDict &headers = {}                             ) { return Send(std::make_shared<TCurlRequest>(uri, option, headers)); }
        inline bool POST  (const char *uri, Option_t *option = "", const TCurlDict &headers = {}, const char *parameters = "") { return Send(std::make_shared<TCurlRequest>(uri, option, headers, TCurlMethod::POST, parameters)); }
        inline bool PUT   (const char *uri, Option_t *option = "", const TCurlDict &headers = {}, const char *parameters = "") { return Send(std::make_shared<TCurlRequest>(uri, option, headers, TCurlMethod::PUT, parameters)); }
        inline bool DELETE(const char *uri, Option_t *option = "", const TCurlDict &headers = {}, const char *parameters = "") { return Send(std::make_shared<TCurlRequest>(uri, option, headers, TCurlMethod::DELETE, parameters)); }

        const char *Checksum() const;
        bool IsBinary() const;

        // Cache related methods
        void Evict(const char *uri) const;

        int  GetTTL() const;
        void SetTTL(int ttl);

        const char *GetPool() const; 
        TString GetPoolDirectory() const;
        void SetPool(const char *pool); 
        void ClearPool();

        static void ClearPool(const char *pool);

        const char *GetError() const;
        int GetErrorCode() const;
        long GetStatusCode() const;

        void Print(Option_t *opt = "");
        void Dump();

        friend std::ostream& operator<<(std::ostream& os, const TCurlFile& curl)
        {
            // Dump file
            std::ifstream src(curl.GetName(), std::ios::binary);
            if (!src) return os;

            struct stat stat_buf;
            if (stat(curl.GetName(), &stat_buf) != 0) return os;
            
            std::string line;
            if(!curl.IsBinary()) {
            
                while (std::getline(src, line)) {
                    os << line << std::endl;
                }
            }

            src.close();
            return os;
        }

        // Progress bar information
        void SetProgressPrefix(const char *);
        void SetProgressSuffix(const char *);
        void SetProgressWidth(int);
        void SetProgressChar(char);
        
    ClassDef(TCurlFile, 1) // TCurlFile class for downloading files from URLs
};
