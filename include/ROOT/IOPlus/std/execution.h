#pragma once 

#include <cstdlib>
#include <execution>
#ifdef STD_PSTLD_EXTEND
    #if defined(__APPLE__) && defined(__MACH__)
        #define PSTLD_HEADER_ONLY   // no prebuilt library, only the header
        #define PSTLD_HACK_INTO_STD // export into namespace std

        #include "ROOT/IOPlus/std/pstld.h"
    #endif
#endif