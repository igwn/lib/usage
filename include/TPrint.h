/**
 * @file TPrint.h
 * @brief Header of the TPrint class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TPrint
 * @brief Description of the TPrint class.
 * @details Detailed description of the TPrint class, its functionality, and usage.
 */

 #pragma once

 #include <Riostream.h>
 
 #include <sys/stat.h>
 #include <map>
 #include <sstream>
 #include <vector>
 
 #include <TSystem.h>
 #include <TH1.h>
 #include <TGraph.h>
 
 #include <TObject.h>
 #include <TLorentzVector.h>
 #include <TPRegexp.h>
 #include <TROOT.h>
 #include <TObjString.h>
 #include <TObjArray.h>
 #include <unistd.h>
 #include <sstream>
 #include <sys/ioctl.h>
 
 #include "TKeyboard.h"
 
 #include "ROOT/IOPlus/Helpers.h"
 #include <chrono>
 
 #ifdef _WIN32
 #include <conio.h>
 #else
 #include <unistd.h>
 #include <termios.h>
 #endif
 
 #include <cstdarg>
 #include <cstdio>
 #include <cstdlib>
 
 // abi:cxx dependency
 #include <cxxabi.h>
 
 // Add a defined function to get the method name
 inline TString MethodName(const TString& prettyFunction)
 {
         TString function_name = prettyFunction;
         TString function_args = prettyFunction;
         int colons = function_name.First('(');
 
         function_name = function_name(0, colons);
         int space = function_name.Last(' ');
         function_name = function_name(space+1, function_name.Length()-space+1);
 
         function_args = function_args(colons, function_args.Length()-colons+1);
 
         function_name = (TString) function_name.Strip(TString::kBoth, '*');
         return function_name.EqualTo("main") ? "Main" : function_name;
 }
 
 #define __METHOD_NAME__ MethodName(__PRETTY_FUNCTION__)
 #define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters
 
 class TPrint
 {
        private:

                static int     OpenTTY(int desc, TString fname = "");
                static TString CloseTTY(int desc);
                static bool    ValidTTY(TString fname);
                static TString ReadTTY(TString fname);
 
        public:
 
                static int iTTY;
                static int iFailedTTY;
                static int kStdOut_dup;
                static int kStdErr_dup;
                static std::vector<int> kStdOut;
                static std::vector<TString> kStdOut_fname;
                static std::vector<int> kStdErr;
                static std::vector<TString> kStdErr_fname;
        
                static std::vector<TString> kTrash;
        
                static std::vector<int> asserts;
                static TString CreateTemporaryPath(TString format = "/tmp/tmp.XXXXXX");
                
                inline static void ReadAndWrite(TString fname, TString regex_pattern, TString replacement) { TPrint::ReadAndWrite(fname, std::vector<TString>({regex_pattern}), std::vector<TString>({replacement})); }
                        static void ReadAndWrite(TString fname, std::vector<TString> regex_pattern, std::vector<TString> replacement);
                
                inline static bool FileExists (const char *name) {
                        struct stat buffer;
                        return (stat (name, &buffer) == 0);
                }
        
                // Main variables
                static int kDebug;
                static bool kQuiet, kSuperQuiet;
                static bool kCarriageReturn;
        
                static bool bShortRedirectTTY;
                static const bool kIgnoreTab;
                static const bool kIncrementTab;
                
                static const TString kNoColor;
                static const TString kUnchangedColor;
                static const TString kRed;
                static const TString kGreen;
                static const TString kOrange;
                static const TString kOrangeBlink;
                static const TString kBlue;
                static const TString kPurple;
        
                static bool IsBatch();
                static bool IsInteractive();

                // Constructor, destructor
                TPrint();
                ~TPrint();
        
                static void EmptyTrash() {
        
                        while(kTrash.size() > 0) {
        
                                if(FileExists(kTrash.back())) remove(kTrash.back());
                                kTrash.pop_back();
                        }
                }
        
                static TString Trim(const char* str) {
        
                        TString str0 = str;
                        while(str0.BeginsWith("\n") || str0.BeginsWith(" ") || str0.BeginsWith("\r") || str0.BeginsWith("\t")) str0 = str0(1, str0.Length());
                        while(str0.EndsWith("\n") || str0.EndsWith(" ") || str0.EndsWith("\r") || str0.EndsWith("\t")) str0 = str0(0, str0.Length()-1);
        
                        return str0;
                }
        
                static int kTabular;
                static const char kTabularChar;
        
                static bool IsQuiet() {
                        return TPrint::kQuiet || TPrint::kSuperQuiet;
                }
                static bool IsSuperQuiet() {
                        return TPrint::kSuperQuiet;
                }
        
                static TString Endl()
                {
                        return ( TPrint::IsQuiet() ) ? "" : TString('\n');
                }
        
                template <typename T>
                static TString Address(T *ptr)
                {
                        const void * address = static_cast<const void*>(ptr);
                        std::stringstream ss; ss << address;  
                        return TString(ss.str()); 
                }
                
                static TString CarriageReturn(bool bClean = false)
                {
                        return TString(bClean ? "\33[2K\r" : "\r");
                }
        
                static TString ScrollUp(int lines = 1)
                {
                        return TString("\33[" + TString::Itoa(lines, 10) + "A");
                }
        
                static TString Readlink(TString fname) {
        
                        char buff[1024], buff2[1024];
                        for(int i = 0, N = fname.Length(); i < N; i++)
                                buff[i] = fname[i];
        
                        buff[fname.Length()] = '\0';
        
                        memset(buff2, 0, sizeof(buff2));
                        if(readlink(buff, buff2, sizeof(buff2)-1) < 0) strcpy(buff2,buff);
        
                        realpath(buff2, buff);
                        return TString(buff);
                }
        
                static TString Readlink(int fd) {
        
                        if(fd < 0) return "";
        
                        char buff[1024];
                        snprintf(buff, 1024, "/proc/self/fd/%d", fd);
        
                        TString fname = TString(buff);
                        return Readlink(fname);
                }
        
                static TString Readlink(FILE *f) {
        
                        return Readlink(fileno(f));
                }
        
                static bool bSingleSkipIncrementTab;
                static void SetSingleSkipIncrementTab() {
                        bSingleSkipIncrementTab = true;
                }
                static bool bSingleCarriageReturn;
                static void SetSingleCarriageReturn() {
                        bSingleCarriageReturn = true;
                }
        
                static void ClearLine(int nlines = 1) {
        
                        while(nlines-- > 0) {

                                std::cout << TPrint::CarriageReturn(true);
                                if(nlines > 0) std::cout << TPrint::ScrollUp();
                        }

                        std::cout << std::flush;
                        SetSingleCarriageReturn();
                }
        
                template<typename... Args> 
                static int WaitForKeyPress(const char* title, const std::vector<std::vector<unsigned char>> &keycodes, const char* highlight = "", const char *content = "", Args... args) { return WaitForKeyPress(0, title, keycodes, highlight, content, args...); }
                template<typename... Args> 
                static int WaitForKeyPress(const char* title, const std::vector<unsigned char> &keycodes, const char* highlight = "", const char *content = "", Args... args) { return WaitForKeyPress(0, title, {keycodes}, highlight, content, args...); }
                template<typename... Args> 
                static int WaitForKeyPress(const char* title, const unsigned char &keycode, const char* highlight = "", const char *content = "", Args... args) { return WaitForKeyPress(0, title, {{keycode}}, highlight, content, args...); }
        
                static int WaitForKeyPress(int, const char* title, const std::vector< std::vector<unsigned char>> &keycodes, const char* highlight = "", const char *content = "", ...);
                
                template<typename... Args> 
                static void PressAnyKeyToContinue(const char* title, const char* content = "", Args... args) { return PressAnyKeyToContinue(0, title, content, args...); }

                static void PressAnyKeyToContinue(int, const char*, const char* = "", ...);
        
                static std::chrono::time_point<std::chrono::system_clock> progressBarTime;
                static TString GetProgressBar(const char*, int, int, int = 1);
                static void  ProgressBar(const char*, Option_t*, int, int, int = 1);
                
                static TString GetDate(const std::chrono::time_point<std::chrono::system_clock>& tp, const TString& = "%H:%M:%S");
                static TString GetDate(long long timestamp, const TString& format = "%H:%M:%S");
                static TString GetTime(const std::chrono::time_point<std::chrono::system_clock>& tp, const TString& format = "%Hh %Mm %Ss");
                static TString GetTime(long long timestamp, const TString& format = "%Hh %Mm %Ss");
        
                static std::map<TString, std::chrono::high_resolution_clock::time_point> onceX;
                static bool OnceEvery(int ms, const char *file, int line);
        
                static TString GetTestBar(const char*);
                static void  TestBar(const char*);
                static bool  RunTests    (const char*);
                static void  Tests  (const char*);
                static void  ScheduleTests(int N);
                static void  Assert(const char*, bool b, int pos = -1);
        
                static void ResetTab();
                static void IncrementTab();
                static void DecrementTab();
                static TString Tab();
        
                static int CharCount(int N) { return CharCount(TString::Itoa(N, 10)); }
                static int CharCount(TString str) { return str.Length(); }
        
                static TString GetInfo(const char*, const char*, ...);
                static void Info(const char*, const char*, ...);
        
                static TString GetMessage(const char*, const char*, ...);
                static void Message(const char*, const char*, ...);
        
                inline static bool IsDebugEnabled(int level = 1) {
                        return TPrint::kDebug >= level;
                };
        
                static void Debug(const char* title, TLorentzVector& lv, bool b = true) { return Debug(1, title, lv, b); };
                static void Debug(const char* title, TVector3& v3, bool b = true) { return Debug(1, title, v3, b); };
                static void Debug(const char* title, const char* str0,...)
                {
                        va_list aptr;
                        va_start(aptr, str0);
                        TString str = ReplaceVariadic(str0, aptr);
        
                        return Debug(1, title, str);
                }
        
                static void Debug(int, const char*, const char*, ...);
                static void Debug(int, const char*, TLorentzVector&, bool = true);
                static void Debug(int, const char*, TVector3&, bool = true);
        
                template <typename T>
                static void Debug(int, const char*, std::vector<T>);
                template <typename T>
                static void Debug(int, const char*, std::vector< std::vector<T>>);
        
                template <typename T>
                static void Debug(const char* title, std::vector<T> v);
                template <typename T>
                static void Debug(const char* title, std::vector< std::vector<T>> vv);
        
                template<typename T>
                static TString GetType(bool demangle = true);
                static std::vector<TString> GetTypename(TString prettyFunction);
        
                static TString ReplaceVariadic(const char*, va_list);
                static TString GetError(const char*, const char*, ...);
                static void Error(const char*, const char*, ...);
                static bool ErrorIf(bool, const char*, const char*, ...);
        
                static TString GetWarning(const char*, const char*, ...);
                static void Warning(const char*, const char*, ...);
                static bool WarningIf(bool, const char*, const char*, ...);
        
                static TString ListTTY(int desc);
                static void GetTTY(const char *, TString *, TString *);
                static int RedirectTTY(const char*, bool = true);
                static void DumpTTY(const char*);
        
                static void TTY(const char*, bool = true, bool = true);
                static void TTY(const char* title, const char*, const char* = "");
        
                static void ClearPage();
                static void ErasePreviousLines(int previousLines);
                static void ErasePreviousLine() { ErasePreviousLines(1); }
                
                static TString Redirect(TObject*, Option_t* = "");
        
                static std::vector<double> envcoeffmap;
                static std::vector<TString> envhashmap;
                static std::map<TString, TString> envlist;
        
                static TString MakeItShorter(TString, int = -1, TString = TPrint::kNoColor, TString = TPrint::kRed + "[..]" + TPrint::kNoColor);
                static TString ExpandVariables(TString, bool = true);
                static void PrepareEnvList(bool = false);
                static void PrintMessageEveryXSeconds(int x);
        
        ClassDef(TPrint, 1);
 };
 
 template<typename T>
 TString TPrint::GetType(bool demangle) {
 
        TString name = typeid(T).name();
        if(!demangle) return name;
        
        int status;
        char* demangled = abi::__cxa_demangle(name, nullptr, nullptr, &status);

        if (status == 0) {

                TString result(demangled);
                        result.ReplaceAll("__1::", ""); // clang namespace
                        
                free(demangled);
                return result;
        }
        
        return "";
 }
 
 template <typename T>
 void TPrint::Debug(const char* title, std::vector<T> v) { return Debug(1, title, v); }
 
 template <typename T>
 void TPrint::Debug(const char* title, std::vector< std::vector<T>> vv) { return Debug(1, title, vv); }
 
 template <typename T>
 void TPrint::Debug(int iDebugLevel, const char* title, std::vector<T> a)
 {
        if(!a.size()) {

                TPrint::Debug(iDebugLevel, title, "Vector is empty.. skip");
                return;
        }

        TString os;
        if( TPrint::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {

                TString head = (!TString(title).EqualTo("")) ? "Debug (lvl " + TString::Itoa(iDebugLevel,10) + ") from <" + TString(title) + ">: " : "";

                if(TString(title).EqualTo("")) std::cout << TPrint::Tab();
                else std::cout << TPrint::Tab() << TPrint::kPurple + MakeItShorter(head) +TPrint::kNoColor;

                std::cout << Form("v.size() = %d; simple vector\n%sv = (", (int) a.size(), TString(ROOT::IOPlus::Helpers::Spacer(head.Length())).Data());
                for(int i = 0, N = a.size(); i < N; i++) {

                        std::stringstream ss;
                                        ss << std::setw(8);
                                        ss << std::left << std::fixed << std::setprecision(2);

                        if(i != (int) a.size()-1) ss << ((TString)"\"" + a[i] + "\"") << ", ";
                        else ss << ((TString)"\"" + a[i] + "\"");

                        std::cout << ss.str();
                        if(i!=(int) a.size()-1 && (i+1)%8 == 0) std::cout << "\n" << ROOT::IOPlus::Helpers::Spacer(head.Length()) << "     ";

                }
                std::cout << ")" << std::endl;
        }
 }
 
 template <typename T>
 void TPrint::Debug(int iDebugLevel, const char* title, std::vector< std::vector<T>> a)
 {
         if(!a.size()) {
 
                 TPrint::Debug(iDebugLevel, title, "Vector is empty.. skip");
                 return;
         }
 
         TString os;
         if( TPrint::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {
 
                 TString head = (!TString(title).EqualTo("")) ? "Debug (lvl " + TString::Itoa(iDebugLevel,10) + ") from <" + TString(title) + ">: " : "";
 
                 if(TString(title).EqualTo("")) std::cout << TPrint::Tab() << std::endl;
                 else std::cout << TPrint::Tab() << TPrint::kPurple + MakeItShorter(head) +TPrint::kNoColor << std::endl;
 
                 std::cout << TPrint::Tab() << Form("v.size() = %d; matrix", (int) a.size()) << std::endl;
                 std::cout << TPrint::Tab() << "v = (";
                 for(int i = 0, N = a.size(); i < N; i++) {
 
                         std::cout << std::endl << TPrint::Tab() << "       (";
 
                         for(int j = 0, J = a[i].size(); j < J; j++) {
 
                                 std::stringstream ss;
                                 ss << std::setw(10);
                                 ss << std::fixed << std::setprecision(2) << std::left;
                                 if(j != (int) a[i].size()-1) ss << ((TString)"\"" + a[i][j]) << ", ";
                                 else ss << ((TString)"\"" + a[i][j] + "\" ");
 
                                 std::cout << ss.str();
                         }
 
                         if(i != (int) a.size()-1) std::cout << "),";
                         else std::cout << ")";
                 }
                 std::cout << std::endl << TPrint::Tab() << ")" << std::endl;
         }
 }
 
 R__EXTERN TPrint *gPrint;