/**
 * @file TPicasso.h
 * @brief Header of the TPicasso class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TPicasso
 * @brief Description of the TPicasso class.
 * @details Detailed description of the TPicasso class, its functionality, and usage.
 */

#pragma once

#include <Rioplus.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TPDF.h>
#include <TTree.h>

class TPicasso
{
       public:
        TPicasso() { };
       ~TPicasso() { };

       int GetDisplayWidth() const;
       int GetDisplayHeight() const;
       
       static TCanvas *MakeCornerPlot(int Nx, int Ny);
       TCanvas *DrawCornerPlot(TTree *t, const char *varexp, const char *selection = "", Option_t *option = "", Long64_t nentries = TTree::kMaxEntries, Long64_t firstentry = 0);
       
       static TCanvas *MakeMatrixPlot(int Nx, int Ny);
       TCanvas *DrawMatrixPlot(const std::vector<std::vector<TH1*>> &matrix);
       TCanvas *DrawMatrixPlot(TTree *t, const char *varexp, const char *selection = "", Option_t *option = "", Long64_t nentries = TTree::kMaxEntries, Long64_t firstentry = 0);
       
       static TCanvas *MakeRatioPlot();
       TCanvas *DrawRatioPlot(TList *l);

       static TCanvas *MakeSkymap();
       TCanvas *DrawSkymap(TH2D *map, TList *contourList = NULL);

       static TCanvas *MakeJointPlot();
       TCanvas *DrawJointPlot(TList *l, Option_t *option = "");

       static TList *GetListOfObjects(TCanvas *c0);
       static TString GetTitle(TCanvas *c);

       TCanvas *MergeCanvas(TCanvas *c0, TCanvas *c1);
       TCanvas *MergeCanvas(TList *l);
       inline bool Write(TString output, TCanvas* canvas);
              bool Write(TString output, std::vector<TCanvas*> &canvases);
       
       ClassDef(TPicasso,1);
};

R__EXTERN TPicasso *gPicasso;
