#include "TCurl.h"
#include <iostream>

int main(int argc, char **argv)
{  
    TString url = "https://example.com"; // Replace with your URL
    TString destination = "file.txt"; // Destination file path

    // Set cache TTL to 1 hour (3600 seconds)
    TCurl myFile(destination, "RECREATE", 3600);
    if ( myFile.Fetch(url) ) {
        
        myFile.Print();
        myFile.Dump();
    }

    return 0;
}
